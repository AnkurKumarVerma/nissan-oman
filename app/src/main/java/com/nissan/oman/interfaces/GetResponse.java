package com.nissan.oman.interfaces;

/**
 * Created by ankur on 3/29/16.
 */
public interface GetResponse {

  void  getResponse(String error, String response);

}
