package com.nissan.oman;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

import com.nissan.oman.utils.SharedPrefKeys;
import com.nissan.oman.utils.SharedPreferenceUtil;


public class SplashScreenActivity extends Activity {


    private static final int SPLASH_SCREEN_MIN_VISIBLE_TIME = 1; // seconds
    private SharedPreferenceUtil sharedPreferenceUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_splash_screen);

        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(SplashScreenActivity.this);

        getDisplayWidth(SplashScreenActivity.this);
        getDisplayHeight(SplashScreenActivity.this);

        IntentLauncher launcher = new IntentLauncher();
        launcher.start();
    }

    private void getDisplayWidth(Activity a) {

        Display display = a.getWindowManager().getDefaultDisplay();

        // creating an empty Point so that the compiler
        // does not complain about null reference
        Point displaySize = new Point();
        display.getSize(displaySize);
        sharedPreferenceUtil.setScreenWidth(displaySize.x);
    }

    private void getDisplayHeight(Activity a) {

        Display display = a.getWindowManager().getDefaultDisplay();

        // creating an empty Point so that the compiler
        // does not complain about null reference
        Point displaySize = new Point();
        display.getSize(displaySize);
        sharedPreferenceUtil.setScreenHeight(displaySize.y);


    }

    private class IntentLauncher extends Thread {
        @Override
        /**
         * Sleep for some time and than start new activity.
         */
        public void run() {


            boolean isVarified = sharedPreferenceUtil.getData(
                    SharedPrefKeys.IS_VARIFIED, false);

            if (isVarified) {

                try {
                    // Sleeping
                    Thread.sleep(SPLASH_SCREEN_MIN_VISIBLE_TIME * 1000);
                } catch (Exception e) {
                    System.out.println("error" + e.getMessage());
                }
                Intent intent = new Intent(SplashScreenActivity.this,
                        MainActivity.class);
                SplashScreenActivity.this.startActivity(intent);
                SplashScreenActivity.this.finish();

            } else {
                try {
                    // Sleeping
                    Thread.sleep(SPLASH_SCREEN_MIN_VISIBLE_TIME * 1000);
                } catch (Exception e) {
                    System.out.println("error" + e.getMessage());
                }


                Intent intent = new Intent(SplashScreenActivity.this,
                        LoginActivity.class);
                SplashScreenActivity.this.startActivity(intent);
                SplashScreenActivity.this.finish();
            }

        }
    }


}
