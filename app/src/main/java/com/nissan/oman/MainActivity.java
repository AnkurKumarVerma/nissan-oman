package com.nissan.oman;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;

import com.nissan.oman.adapter.NavDrawerListAdapterMenu;
import com.nissan.oman.fragment.ChatFragment;
import com.nissan.oman.fragment.HomeFragment;
import com.nissan.oman.fragment.LocationFragment;
import com.nissan.oman.fragment.MobileFragment;
import com.nissan.oman.fragment.VehicleCarsFragment;
import com.nissan.oman.fragment.VehicleFragment;
import com.nissan.oman.model.NavigationDrawerItem;
import com.nissan.oman.utils.Reachability;
import com.nissan.oman.utils.SharedPreferenceUtil;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TabHost.OnTabChangeListener
{


    private FragmentTabHost mTabHost;

    private Reachability reachability;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private Context mContext;
    private ImageView imgMenu,imgSearch;
    final int[] drawable = {R.drawable.homeicon,
            R.drawable.location_icon,
            R.drawable.vehicles,
            R.drawable.mobile_icon
            };



    private ArrayList<NavigationDrawerItem> navDrawerItems;
    private NavDrawerListAdapterMenu adapter;
    private String[] navMenuTitles;


    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mContext = MainActivity.this;
        init();

        //sendRegId();

        navigationDrawersetUp();
        setActionbar();


    }


    public void init() {
        reachability = Reachability.getInstance(getApplicationContext());
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getApplicationContext());


        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(MainActivity.this, getSupportFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("HOME").setIndicator(getTabIndicator(mContext, "", getResources().getDrawable(drawable[0]))),
                HomeFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("LOCATION").setIndicator(getTabIndicator(mContext,"",getResources().getDrawable(drawable[1]))),
                LocationFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("CARS").setIndicator(getTabIndicator(mContext,"",getResources().getDrawable(drawable[2]))),
                VehicleFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("MOBILES").setIndicator(getTabIndicator(mContext,"",getResources().getDrawable(drawable[3]))),
                MobileFragment.class, null);



        mTabHost.setOnTabChangedListener(this);


        for (int i = 0; i < mTabHost.getTabWidget().getChildCount(); i++) {
            View v = mTabHost.getTabWidget().getChildAt(i);
            //v.setBackgroundColor(getResources().getColor(android.R.color.black));

            Drawable buttonDrawable = mContext.getResources().getDrawable(R.drawable.tab_background);
            buttonDrawable.mutate();
            v.setBackground(buttonDrawable);


        }


    }


    private View getTabIndicator(Context context, String title, Drawable drawable) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_indicator, null);
        ImageView iv = (ImageView) view.findViewById(R.id.icon);
        iv.setImageDrawable(drawable);

        return view;
    }





    ActionBar actionBar ;
   // private ViewGroup actionBarLayout;
   ActionBar.LayoutParams lp;
    SearchView searchView;
    Toolbar toolbar;
    private void setActionbar() {


        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);


//        actionBarLayout = (ViewGroup) getLayoutInflater()
//                .inflate(R.layout.action_bar,null);

        actionBar = getSupportActionBar();
//        toolbar.setNavigationIcon(R.drawable.menu);

        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
           // actionBar.setCustomView(toolbar);

//            actionBar.setBackgroundDrawable(new ColorDrawable(Color
//                    .parseColor("#ffffff")));

            searchView = (SearchView)toolbar.findViewById(R.id.search_view);
            ImageView v = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_button);
            v.setColorFilter(Color.WHITE);
            searchView.setEnabled(false);
            searchView.clearFocus();
            searchView.setClickable(false);





            imgMenu = (ImageView) toolbar.findViewById(R.id.imageMenu);
            imgMenu.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            });


            searchView.setOnSearchClickListener(new View.OnClickListener() {
                private boolean extended = false;

                @Override
                public void onClick(View v) {
                    if (!extended) {
                        extended = true;
                        searchView.setIconified(false);
                      //  lp = v.getLayoutParams();
                       // lp.width = android.support.v7.app.ActionBar.LayoutParams.MATCH_PARENT;
                       // list_search.setBackgroundColor(Color.TRANSPARENT);
                    }
                }

            });
//
//
//            if (null != searchView )
//            {
//                // searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//                //searchView.setIconifiedByDefault(false);
//            }
//
//
//            searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
//
//                @Override
//                public void onFocusChange(View v, boolean hasFocus) {
//                    // TODO Auto-generated method stub
//
//                    if(hasFocus)
//
//                    {
//                        if(lp!=null)
//                        {
//                            lp.width = ActionBar.LayoutParams.MATCH_PARENT;
//                        }
//                        imgMenu.setVisibility(View.GONE);
//                        searchView.setBackgroundColor(Color.WHITE);
//                        //list_search.setVisibility(View.VISIBLE);
//
//
//                    }
//                    else
//                    {
//                        if(lp!=null)
//                        {
//                            lp.width = ActionBar.LayoutParams.WRAP_CONTENT;
//                        }
//                        //searchView.setBackgroundColor(Color.BLACK);
//                        //list_search.setBackgroundColor(Color.TRANSPARENT);
//                        //title_backstage.setVisibility(View.VISIBLE);
//                        imgMenu.setVisibility(View.VISIBLE);
//                    }
//                }
//            });
//
//
//            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
//                @Override
//                public boolean onClose() {
//                    if(lp!=null)
//                    {
//                        lp.width = ActionBar.LayoutParams.WRAP_CONTENT;
//                    }
//                    searchView.setIconified(false);
//                    searchView.setBackgroundColor(Color.BLACK);
//
//                    return false;
//                }
//            });
//
//
//
//            SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener()
//            {
//                public boolean onQueryTextChange(String newText)
//                {
//                    //new GetSearchData().execute(newText.trim());
//                    //setAdapterForSearch(newText.trim());
//                    //Toast.makeText(MainActivity.this, "textchange", Toast.LENGTH_SHORT).show();
//                    // this is your adapter that will be filtered
//
//                    //					adapter.getFilter().filter(newText);
//                    //					adapter.notifyDataSetChanged();
//                    //					chatList.invalidate();
//                    return true;
//                }
//
//                public boolean onQueryTextSubmit(String query)
//                {
//                    //imgMenu.setVisibility(View.INVISIBLE);
//
//                    if(query.length()>0)
//                    {
//                        //new GetSearchData().execute(query.trim());
//                        if (!"".equals(query)) {
//                            searchView.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    searchView.clearFocus();
//                                    imgMenu.setVisibility(View.INVISIBLE);
//                                    if(lp!=null)
//                                    {
//                                        lp.width = ActionBar.LayoutParams.MATCH_PARENT;
//                                    }
//
//                                }
//                            });
//                        }
//                    }
//
//
//                    return true;
//                }
//            };
//            searchView.setOnQueryTextListener(queryTextListener);

            //	txtConnected = (TextView) findViewById(R.id.action_bar_connecting);

        }




    }





    private void navigationDrawersetUp() {
        // TODO Auto-generated method stub
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavigationDrawerItem>();

        // adding nav drawer items to array
        // Home
        navDrawerItems.add(new NavigationDrawerItem(navMenuTitles[0]));
        navDrawerItems.add(new NavigationDrawerItem(navMenuTitles[1]));
        navDrawerItems.add(new NavigationDrawerItem(navMenuTitles[2]));

        // Recycle the typed array


        //	mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapterMenu(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        //mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // enabling action bar app icon and behaving it as toggle button
		/*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
				getSupportActionBar().setHomeButtonEnabled(true);
				getSupportActionBar().setIcon(null);
				getSupportActionBar().setDisplayUseLogoEnabled(false);


				getSupportActionBar().setTitle("Backstage");*/




        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.menu, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                //	getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }


            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // TODO Auto-generated method stub
                super.onDrawerSlide(drawerView, 0);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

		/*if (savedInstanceState == null) {
					// on first time display view for first nav item
					displayView(0);
				}*/


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // TODO Auto-generated method stub
                displayView(position);
            }
        });
    }


    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item

            System.out.println("in last"+position);
            displayView(position);

            //	if()

        }
    }


    private void displayView(int position)
    {
        switch (position)
        {
            case 0:
                mDrawerLayout.closeDrawers();
                break;
            case 1:
                mDrawerLayout.closeDrawers();
                break;
            case 2:
                mDrawerLayout.closeDrawers();
                break;
            case 3:


                break;


        }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }




    @Override
    public void onTabChanged(String tabId) {
        // TODO Auto-generated method stub
        System.out.println("tab id" + tabId);

        try {
            FragmentManager manager = getSupportFragmentManager();

            int countValue = manager.getBackStackEntryCount() ;

            if (countValue > 0)
            {
                FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
                manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }

             countValue = manager.getBackStackEntryCount() ;


            System.out.print(countValue);
        }
        catch (Exception e)
        {
            e.getMessage();
        }
        this.tabId = tabId;

        {



            FragmentManager manager = getSupportFragmentManager();




            if(tabId.equalsIgnoreCase("HOME"))
            {
                //if (manager.getBackStackEntryCount() == 0)
                {
                  //  mTabHost.

                    HomeFragment newFragment = new HomeFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.realtabcontent, newFragment);
                    transaction.commit();

                }
            }
            else if(tabId.equalsIgnoreCase("LOCATION"))
            {
                LocationFragment newFragment = new LocationFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.realtabcontent, newFragment);
                transaction.commit();
            }
            else if(tabId.equalsIgnoreCase("CARS"))
            {
                //if (manager.getBackStackEntryCount() == 0)
                {
                    VehicleFragment newFragment = new VehicleFragment();

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.realtabcontent, newFragment);
                    transaction.commit();


                   /* FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.realtabcontent, newFragment);
                    transaction.commit();*/

                   // pushFragments(tabId, new AppTabAFirstFragment(), false,true);
                }
            }
            else if(tabId.equalsIgnoreCase("MOBILES"))
            {
                MobileFragment newFragment = new MobileFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                transaction.replace(R.id.realtabcontent, newFragment);
                transaction.commit();
            }
        }
    }




    String tabId = "Home";

    @Override
    public void onBackPressed() {

        if(tabId.equalsIgnoreCase("HOME"))
        {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                finish();
            }
        }
        else if(tabId.equalsIgnoreCase("LOCATION"))
        {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                finish();
            }
        }
        else if(tabId.equalsIgnoreCase("CARS"))
        {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                finish();
            }
        }
        else if(tabId.equalsIgnoreCase("MOBILES"))
        {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                finish();
            }
        }
    }
}
