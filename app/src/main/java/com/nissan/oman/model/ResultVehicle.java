package com.nissan.oman.model;

import java.util.ArrayList;

/**
 * Created by jyoti on 5/12/16.
 */
public class ResultVehicle
{
    private String category;
    private ArrayList<CarList> carlist=new ArrayList<>();

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<CarList> getCarlist() {
        return carlist;
    }

    public void setCarlist(ArrayList<CarList> carlist) {
        this.carlist = carlist;
    }
}
