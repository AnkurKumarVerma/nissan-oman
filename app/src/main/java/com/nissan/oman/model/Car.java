package com.nissan.oman.model;

import java.util.ArrayList;

/**
 * Created by ankur on 5/2/16.
 */
public class Car {

    private String name;
    private String overview;
    private String exterior;
    private String interior;
    private String safety;
    private String technology;
    private String specifications;
    private String color_trim;
    private ArrayList<String> gallery;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getExterior() {
        return exterior;
    }

    public void setExterior(String exterior) {
        this.exterior = exterior;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    public String getSafety() {
        return safety;
    }

    public void setSafety(String safety) {
        this.safety = safety;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getColor_trim() {
        return color_trim;
    }

    public void setColor_trim(String color_trim) {
        this.color_trim = color_trim;
    }

    public ArrayList<String> getGallery() {
        return gallery;
    }

    public void setGallery(ArrayList<String> gallery) {
        this.gallery = gallery;
    }
}
