package com.nissan.oman.model;

import java.util.ArrayList;

/**
 * Created by ankur on 5/3/16.
 */
public class Vehicles {

    private ArrayList<CarType> carTypeArrayList;
    private ArrayList<String> gallery;

    public ArrayList<CarType> getCarTypeArrayList() {
        return carTypeArrayList;
    }

    public void setCarTypeArrayList(ArrayList<CarType> carTypeArrayList) {
        this.carTypeArrayList = carTypeArrayList;
    }

    public ArrayList<String> getGallery() {
        return gallery;
    }

    public void setGallery(ArrayList<String> gallery) {
        this.gallery = gallery;
    }
}
