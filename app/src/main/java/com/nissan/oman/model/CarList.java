package com.nissan.oman.model;

/**
 * Created by jyoti on 5/12/16.
 */
public class CarList
{


    private String vehicle_name;
    private String vehicle_overview;
    private String vehicle_exterior;
    private String exterior_pic;
    private String vehicle_interior;
    private String interior_pic;
    private String engine;
    private String trans;


    public String getVehicle_name() {
        return vehicle_name;
    }

    public void setVehicle_name(String vehicle_name) {
        this.vehicle_name = vehicle_name;
    }

    public String getVehicle_overview() {
        return vehicle_overview;
    }

    public void setVehicle_overview(String vehicle_overview) {
        this.vehicle_overview = vehicle_overview;
    }

    public String getVehicle_exterior() {
        return vehicle_exterior;
    }

    public void setVehicle_exterior(String vehicle_exterior) {
        this.vehicle_exterior = vehicle_exterior;
    }

    public String getExterior_pic() {
        return exterior_pic;
    }

    public void setExterior_pic(String exterior_pic) {
        this.exterior_pic = exterior_pic;
    }

    public String getVehicle_interior() {
        return vehicle_interior;
    }

    public void setVehicle_interior(String vehicle_interior) {
        this.vehicle_interior = vehicle_interior;
    }

    public String getInterior_pic() {
        return interior_pic;
    }

    public void setInterior_pic(String interior_pic) {
        this.interior_pic = interior_pic;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }


}
