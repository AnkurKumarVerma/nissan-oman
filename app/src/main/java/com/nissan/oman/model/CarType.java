package com.nissan.oman.model;

import java.util.ArrayList;

/**
 * Created by ankur on 5/3/16.
 */
public class CarType {

    private ArrayList<Car> carsList;

    private ArrayList<String> gallery;

    public ArrayList<Car> getCarsList() {
        return carsList;
    }

    public void setCarsList(ArrayList<Car> carsList) {
        this.carsList = carsList;
    }

    public ArrayList<String> getGallery() {
        return gallery;
    }

    public void setGallery(ArrayList<String> gallery) {
        this.gallery = gallery;
    }
}
