package com.nissan.oman.model;

/**
 * Created by jyoti on 5/5/16.
 */
public class Performance
{

    private String heading;
    private String subHeading;

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public void setSubHeading(String subHeading) {
        this.subHeading = subHeading;
    }
}
