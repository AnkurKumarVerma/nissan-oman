package com.nissan.oman.model;

/**
 * Created by jyoti on 5/2/16.
 */
public class HomeItem
{
    private int icon;
    private String  Heading;
    private int next;
    private int actualIconCar;


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getHeading() {
        return Heading;
    }

    public void setHeading(String heading) {
        Heading = heading;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }
    public int getActualIconCar() {
        return actualIconCar;
    }

    public void setActualIconCar(int actualIconCar) {
        this.actualIconCar = actualIconCar;
    }
}
