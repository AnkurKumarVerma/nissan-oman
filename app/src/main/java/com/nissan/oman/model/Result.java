package com.nissan.oman.model;

/**
 * Created by jyoti on 5/4/16.
 */
public class Result
{
    private String tag;
    private String register;
    private int success;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    private int error;
    private String error_msg;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }





    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
