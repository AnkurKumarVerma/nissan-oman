package com.nissan.oman.utils;

public class SharedPrefKeys {


    public static final String IS_VARIFIED = "is_varified";
    public static final String IS_LOGIN = "is_login";
    public static final String IS_REGISTER= "is_register";

    public static final String USER_PROFILEDATA= "user_profiledata";
    public static final String VEHICLE_DATA= "vehicle_edata";

}
