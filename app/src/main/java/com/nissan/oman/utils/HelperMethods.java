package com.nissan.oman.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by jyoti on 11/4/15.
 */
public class HelperMethods {


    public boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() < 11;
    }


    public boolean isValidEmail(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public String getPerfectMobileNo(String mobileNo) {
        String perfectMobileNo = "";


        char[] mobileCarArray = mobileNo.toCharArray();

        for (int i = 0; i < mobileCarArray.length; i++) {
            if (Character.isDigit(mobileCarArray[i])) {
                perfectMobileNo = perfectMobileNo + mobileCarArray[i];
            }
        }

        return perfectMobileNo;
    }

    public void hideKeyBoard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(activity
                    .getCurrentFocus().getWindowToken(), 0);

            activity.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        } catch (Exception e) {

        }

		/*
         * getWindow().setSoftInputMode(
		 * WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		 */
    }


//    public static String getAppDirPath() {
//        File appStorageDir = new File(Environment.getExternalStorageDirectory(),
//                APPConstants.STORAGEDIR);
//        return appStorageDir.getAbsolutePath();
//    }


    public boolean isPhoneNumberValid(String phoneno) {
        //TODO: Replace this with your own logic
        return phoneno.length() == 10;
    }

    public boolean isConnectedtoNetwork(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] netInfo = cm.getAllNetworkInfo();

        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                if (ni.isConnected()) {
                    haveConnectedWifi = true;
                }
            }

            if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                if (ni.isConnected()) {
                    haveConnectedMobile = true;
                }
            }
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


}
