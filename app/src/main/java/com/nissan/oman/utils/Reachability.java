package com.nissan.oman.utils;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

import java.util.List;


public class Reachability extends BroadcastReceiver {
    public static final int SPEED_50_100K = TelephonyManager.NETWORK_TYPE_1xRTT;
    public static final int SPEED_14_64K = TelephonyManager.NETWORK_TYPE_CDMA;
    public static final int SPEED_50_100K_EDGE = TelephonyManager.NETWORK_TYPE_EDGE;
    public static final int SPEED_400_1000K = TelephonyManager.NETWORK_TYPE_EVDO_0;
    public static final int SPEED_600_1400K = TelephonyManager.NETWORK_TYPE_EVDO_A;
    public static final int SPEED_40_50K = TelephonyManager.NETWORK_TYPE_GPRS;
    public static final int SPEED_2_14M = TelephonyManager.NETWORK_TYPE_HSDPA;
    public static final int SPEED_700_1700K = TelephonyManager.NETWORK_TYPE_HSPA;
    public static final int SPEED_1_23M = TelephonyManager.NETWORK_TYPE_HSUPA;
    public static final int SPEED_400_7000K = TelephonyManager.NETWORK_TYPE_UMTS;
    public static final int UNKNOWN_MOBILE_NETWORK = TelephonyManager.NETWORK_TYPE_UNKNOWN;
    public static final int WIFI_NETWORK = 30;
    private static Reachability singletonObj = null;
    private final String TAG = null;
    private ConnectivityManager connMgr;
    private NetworkInfo networkInfo;
    private Context c;

    public Reachability() {

        singletonObj = this;

    }

    public static Reachability getInstance(Context context) {
        if (singletonObj == null) {
            //Logger.warn("Entered singleton null", "Not Acceptable");
            Reachability reachability = new Reachability();
            reachability.updateNetworkInfoObject(context);
            return reachability;
        }

        return singletonObj;
    }

    @Override
    public void onReceive(Context context, Intent arg) {

        // TODO Auto-generated method stub
        c = context;

        updateNetworkInfoObject(context);


    }

    private void updateNetworkInfoObject(Context context) {
        //if (connMgr == null)
        {
            connMgr = (ConnectivityManager) context.getApplicationContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
        }
    }

//	public boolean isConnectedtoNetwork() {
//		// TODO Auto-generated method stub
//		if (networkInfo != null && networkInfo.isConnected()) {
//
//			return true;
//		}
//		return false;
//
//	}

    public boolean isConnectedtoNetwork(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo[] netInfo = cm.getAllNetworkInfo();

        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                if (ni.isConnected()) {
                    haveConnectedWifi = true;
                }
            }

            if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                if (ni.isConnected()) {
                    haveConnectedMobile = true;
                }
            }
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    @SuppressLint("InlinedApi")
    public int getNetworkType() {

        if (ConnectivityManager
                .isNetworkTypeValid(ConnectivityManager.TYPE_WIFI)) {
            // Toast.makeText(c, str, Toast.LENGTH_LONG).show();
            return ConnectivityManager.TYPE_WIFI;
        } else if (ConnectivityManager
                .isNetworkTypeValid(ConnectivityManager.TYPE_MOBILE)) {
            // Toast.makeText(c, str, Toast.LENGTH_LONG).show();
            return ConnectivityManager.TYPE_MOBILE;

        } else if (ConnectivityManager
                .isNetworkTypeValid(ConnectivityManager.TYPE_MOBILE_DUN)) {
            // Toast.makeText(c, str, Toast.LENGTH_LONG).show();
            return ConnectivityManager.TYPE_MOBILE_DUN;

        } else if (ConnectivityManager
                .isNetworkTypeValid(ConnectivityManager.TYPE_MOBILE_HIPRI)) {
            // Toast.makeText(c, str, Toast.LENGTH_LONG).show();
            return ConnectivityManager.TYPE_MOBILE_HIPRI;

        } else if (ConnectivityManager
                .isNetworkTypeValid(ConnectivityManager.TYPE_MOBILE_MMS)) {
            // Toast.makeText(c, str, Toast.LENGTH_LONG).show();
            return ConnectivityManager.TYPE_MOBILE_MMS;

        } else if (ConnectivityManager
                .isNetworkTypeValid(ConnectivityManager.TYPE_MOBILE_SUPL)) {
            // Toast.makeText(c, str, Toast.LENGTH_LONG).show();
            return ConnectivityManager.TYPE_MOBILE_SUPL;

        } else if (ConnectivityManager
                .isNetworkTypeValid(ConnectivityManager.TYPE_WIMAX)) {
            // Toast.makeText(c, str, Toast.LENGTH_LONG).show();
            return ConnectivityManager.TYPE_WIMAX;
        } else
            return -1;

    }

    private int getNetworkStrength() {
        int netStrength = -1;
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {

            return -1;
        } else {
            int netType = networkInfo.getType();
            int netSubtype = networkInfo.getSubtype();

            if (netType == ConnectivityManager.TYPE_WIFI) // Get wifi strength
            {
                int i;

                WifiManager wifiManager = (WifiManager) c
                        .getSystemService(c.WIFI_SERVICE);
                List<ScanResult> scanResult = wifiManager.getScanResults();
                for (i = 0; i < scanResult.size(); i++) {

                    netStrength = scanResult.get(i).level;
                }

                return netStrength;

            } else if (netType == ConnectivityManager.TYPE_MOBILE) {

                switch (netSubtype) {
                    case TelephonyManager.NETWORK_TYPE_1xRTT:
                        return SPEED_50_100K;

                    case TelephonyManager.NETWORK_TYPE_CDMA:
                        return SPEED_14_64K;

                    case TelephonyManager.NETWORK_TYPE_EDGE:
                        return SPEED_50_100K_EDGE;

                    case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        return SPEED_400_1000K;

                    case TelephonyManager.NETWORK_TYPE_EVDO_A:
                        return SPEED_600_1400K;

                    case TelephonyManager.NETWORK_TYPE_GPRS:
                        return SPEED_40_50K;

                    case TelephonyManager.NETWORK_TYPE_HSDPA:
                        return SPEED_2_14M;

                    case TelephonyManager.NETWORK_TYPE_HSPA:
                        return SPEED_700_1700K;

                    case TelephonyManager.NETWORK_TYPE_HSUPA:
                        return SPEED_1_23M;

                    case TelephonyManager.NETWORK_TYPE_UMTS:
                        return SPEED_400_7000K;

                    case TelephonyManager.NETWORK_TYPE_UNKNOWN: // Unknown
                        return UNKNOWN_MOBILE_NETWORK;

                    default:
                        return -1;
                }

            }

        }
        return -1;
    }

}
