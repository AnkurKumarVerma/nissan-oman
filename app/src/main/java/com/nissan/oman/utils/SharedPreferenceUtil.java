package com.nissan.oman.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferenceUtil {
    private static final String PREF_NAME = "SETTINGS_PREF_FILE";
    private static SharedPreferenceUtil SharedPreferenceUtil;
    private final String KEYBOARDHEIGHT = "KEYBOARDHEIGHT";
    private final String SCREEN_WIDTH = "SCREEN_WIDTH";
    private final String SCREEN_HEIGHT = "SCREEN_HEIGHT";
    private SharedPreferences sharedPreferences;
    private Editor editor;


    public SharedPreferenceUtil() {
        // TODO Auto-generated constructor stub
    }

    private static void initializeNaukriSharedPref(Context context) {
        if (context != null) {
            SharedPreferenceUtil = new SharedPreferenceUtil();
            SharedPreferenceUtil.sharedPreferences = context
                    .getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE
                            | Context.MODE_MULTI_PROCESS);
            SharedPreferenceUtil.editor = SharedPreferenceUtil.sharedPreferences
                    .edit();
        }
    }

    public static SharedPreferenceUtil getInstance(Context context) {
        if (SharedPreferenceUtil == null) {
            initializeNaukriSharedPref(context);
        }
        // initializeNaukriSharedPref(context);
        return SharedPreferenceUtil;
    }

    public synchronized boolean saveData(String key, String value) {
        editor.putString(key, value);
        return editor.commit();
    }

    public synchronized boolean saveData(String key, boolean value) {
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public synchronized boolean saveData(String key, long value) {
        editor.putLong(key, value);
        return editor.commit();
    }

    public synchronized boolean saveData(String key, float value) {
        editor.putFloat(key, value);
        return editor.commit();
    }

    public synchronized boolean saveData(String key, int value) {
        editor.putInt(key, value);
        return editor.commit();
    }


    public synchronized boolean removeData(String key) {
        editor.remove(key);
        return editor.commit();
    }

    public synchronized Boolean getData(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public synchronized String getData(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    public synchronized float getData(String key, float defaultValue) {

        return sharedPreferences.getFloat(key, defaultValue);
    }

    public synchronized int getData(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public synchronized long getData(String key, long defaultValue) {
        return sharedPreferences.getLong(key, defaultValue);
    }

    public synchronized void deleteAllData() {

        SharedPreferenceUtil = null;
        editor.clear();
        editor.commit();
    }

    public int getKeyboardHeight() {
        return sharedPreferences.getInt(KEYBOARDHEIGHT, 0);
    }

    public void setKeyboardHeight(int keyheight) {

        editor.putInt(KEYBOARDHEIGHT, keyheight);
        editor.commit();
    }

    public int getScreenHeight() {
        return sharedPreferences.getInt(SCREEN_HEIGHT, 0);
    }

    public void setScreenHeight(int height) {

        editor.putInt(SCREEN_HEIGHT, height);
        editor.commit();
    }

    public int getScreenWidth() {
        return sharedPreferences.getInt(SCREEN_WIDTH, 0);
    }

    public void setScreenWidth(int width) {

        editor.putInt(SCREEN_WIDTH, width);
        editor.commit();
    }


    public String getUserName(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void setUsername(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }
}
