package com.nissan.oman.touchEvent;

import android.view.View;

/**
 * Created by jyoti on 4/26/16.
 */
public interface ClickListenerInterface

{
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
