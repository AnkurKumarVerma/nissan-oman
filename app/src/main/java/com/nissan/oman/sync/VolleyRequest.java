package com.nissan.oman.sync;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nissan.oman.interfaces.GetResponse;

import java.io.UnsupportedEncodingException;

/**
 * Created by ankur on 3/29/16.
 */
public class VolleyRequest {

    private static RequestQueue mRequestQueue;

    private static VolleyRequest volleyRequest;
    private GetResponse getResponse;


    private int TIME_OUT = 60000;

    public static VolleyRequest getvolleyRequest() {
        if (volleyRequest == null) {
            volleyRequest = new VolleyRequest();
        }

        return volleyRequest;
    }

    public void initInterface(Context mContext)
    {
        getResponse = (GetResponse) mContext;
    }

    public void initInterface(Fragment mContext)
    {
        getResponse = (GetResponse) mContext;
    }

    private RequestQueue getRequestQueue(Context mContext) {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext);
        }

        return mRequestQueue;
    }


    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private <T> void addToRequestQueue(Context mContext, Request<T> request, String tag) {
        // set the default tag if tag is empty
        request.setTag(TextUtils.isEmpty(tag));
        getRequestQueue(mContext).add(request);
    }

    /*public void getResponseFromPostLoginMethod(Context mContext, String url, String tag, final String email, final String password)
    {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                //System.out.println("\"");



                getResponse.getResponse(null, response);

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                String mess = "Error";

                try {

                  //  int resCode = error.networkResponse.statusCode;

                  // mess = error.networkResponse.toString();

                    mess = new String(error.networkResponse.data,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if(error instanceof NoConnectionError) {
                    getResponse.getResponse("No internet available", null);
                }
               *//* else if(resCode == 404)
                {
                    getResponse.getResponse(mess, null);
                }
                else
                {*//*
                else {
                    getResponse.getResponse(mess, null);
                }
                //}
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("Username", email);
                params.put("Password", password);

                return params;
            }

        };



        strReq.setRetryPolicy(new DefaultRetryPolicy(
                TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(mContext, strReq, tag);


    }
*/
    public void getResponseFromGetMethod(Context mContext, String url, String tag) {


        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                getResponse.getResponse(null, response);

                System.out.println("response" +response);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                String mess = "Error";
                System.out.println("error" +error);

                int resCode = 0;

                try {

                    resCode = error.networkResponse.statusCode;

                    mess = new String(error.networkResponse.data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.getMessage();
                }


                if (error instanceof NoConnectionError) {
                        getResponse.getResponse("No internet available. Please enable data and try again later.", "NO_INTERNET");
                } else if (resCode == 401 && error instanceof AuthFailureError) {
                    getResponse.getResponse("Your authorization code is not valid. Do you want to login again.", "401");
                } else {
                      getResponse.getResponse(mess, null);
                }


                //   error.networkResponse.

            }
        }) {




/*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<String, String>();

                headers.put("Authorization",AUTH);

                return headers;
            }*/
        };

        strReq.setRetryPolicy(new DefaultRetryPolicy(
                TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        addToRequestQueue(mContext, strReq, tag);

    }

}
