package com.nissan.oman.sync;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;

import com.nissan.oman.url.ApiUrls;

/**
 * Created by jyoti on 5/4/16.
 */
public class VolleyMethods

{


    public ProgressDialog progressDialog;





    public void downloadDataLogin(Context mContext, String email, String password, VolleyRequest volleyRequest) {

        volleyRequest.initInterface(mContext);


        String url = ApiUrls.LOGIN + "&email=" + email + "&password=" + password;

        volleyRequest.getResponseFromGetMethod(mContext, url, "login");


    }


    public void downloadDataSignUp(VolleyRequest volleyRequest, Context mContext, String name, String email, String password, String mobileno, String dob) {

        volleyRequest.initInterface(mContext);

        // http://webisdomservices.in/nissanweb/webservices/users/?tag=register&name=seval&email=mayank@test.com&password=12345&mobile=7503066930&dob=10/10/1992


        String url = ApiUrls.REGISTRATION + "&name=" + name + "&email=" + email + "&password=" + password + "&mobile=" + mobileno + "&dob=" + dob;

        volleyRequest.getResponseFromGetMethod(mContext, url, "register");


    }


    public void downloadDataReuestBrochure(VolleyRequest volleyRequest, Context mContext, String carmodel, String firstname, String lastname, String showroomid, String email, String phone, android.support.v4.app.Fragment fragment) {

        volleyRequest.initInterface(fragment);

        String url = ApiUrls.REQUESTBROCHURE+ "&car_model=" + carmodel + "&first_name=" + firstname + "&last_name=" + lastname + "&showroom_id=" + showroomid + "&email=" + email+ "&phone=" + phone;

        volleyRequest.getResponseFromGetMethod(mContext, url, "request_a_brochure");


    }


    public void downloadDataForgetPassword(Context mContext, String email, VolleyRequest volleyRequest) {

        volleyRequest.initInterface(mContext);


        String url = ApiUrls.FORGETPASSWORD + "&email=" + email;

        volleyRequest.getResponseFromGetMethod(mContext, url, "forget_password");


    }


    public void downloadDataReuestVehicle(VolleyRequest volleyRequest, Context mContext , android.support.v4.app.Fragment fragment) {

        volleyRequest.initInterface(fragment);

        String url = ApiUrls.REQUESTVEHICLE;

        volleyRequest.getResponseFromGetMethod(mContext, url, "view");


    }


}


