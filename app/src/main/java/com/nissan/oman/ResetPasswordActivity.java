package com.nissan.oman;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.nissan.oman.alertMessage.AlertMessage;
import com.nissan.oman.utils.HelperMethods;
import com.nissan.oman.utils.SharedPreferenceUtil;


public class ResetPasswordActivity extends Activity {


    private EditText editNewPassword;
    private EditText editReenterPassword;

    private TextView btnResetPassword;

    private HelperMethods helperMethod;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private AlertMessage alertMessage;


    private String editNewPasswordtxt = "";
    private String editReenterPasswordtxt = "";

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_resetpassword);
        mContext = ResetPasswordActivity.this;
        init();
    }


    public void init() {
        // showHashKey(MainActivity.this);
        helperMethod = new HelperMethods();
        alertMessage = new AlertMessage(mContext);
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getApplicationContext());


        editNewPassword = (EditText) findViewById(R.id.editNewPassword);
        editReenterPassword = (EditText) findViewById(R.id.editReenterPassword);

        btnResetPassword = (TextView) findViewById(R.id.btnResetPassword);


        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPassword();
            }
        });


    }


    public void resetPassword() {
        boolean isError = false;

        editNewPasswordtxt = editNewPassword.getText().toString().trim();
        editReenterPasswordtxt = editReenterPassword.getText().toString().trim();


        if (editNewPasswordtxt.length() <= 0 || editReenterPasswordtxt.length() <= 0) {
            isError = true;
            alertMessage.alertBox("Please enter all required fields", "Validation Error");
        } else if (!editNewPasswordtxt.equalsIgnoreCase(editReenterPasswordtxt)) {
            isError = true;

            alertMessage.alertBox("Your Password does not match", "Error");
        }
        if (!isError) {
           //web service call
            // new LoginUserToCloud().execute();
        }

    }


//
//    private class LoginUserToCloud extends AsyncTask<Void, Void, Integer> {
//
//        private ProgressDialog progressDialog;
//        private String errorMessage;
//        private JsonMethods jsonMethod;
//
//        @Override
//        protected void onPreExecute() {
//            // TODO Auto-generated method stub
//            super.onPreExecute();
//            progressDialog = ProgressDialog.show(mContext, "", "\nPlease wait...", false);
//            progressDialog.setCancelable(false);
//            jsonMethod=new JsonMethods();
//
//        }
//
//
//        @Override
//        protected Integer doInBackground(Void... params) {
//            // TODO Auto-generated method stub
//
//
//            int ErrorCode = 0;
//
//            if (helperMethod.isConnectedtoNetwork(mContext)) {
//
//
//
//                Login login = new Login();
//                login.setPhoneNumber(editPhonetxt);
//
//                login.setPassword(editPasswordtxt);
//
//
//
//                Gson gsonLogin = new GsonBuilder().create();
//
//                String convert = gsonLogin.toJson(login, Login.class);
//
//                String response = jsonMethod.accountLogin(convert);
//                sharedPreferenceUtil.setUsername("UserName",editUserNametxt);
//
//
//                Gson gson = new Gson();
//                JSONObject jsonResponse;
//                try {
//                    jsonResponse = new JSONObject(response);
//                    //sharedPreferenceUtil.saveData(SharedPrefKeys.LOGIN_DATA,jsonResponse.toString());
//                    ResultsLogin results = gson.fromJson(jsonResponse.toString(), ResultsLogin.class);
//
//                    if (results.getStatus().equalsIgnoreCase("OK")) {
//                        sharedPreferenceUtil.saveData(SharedPrefKeys.LOGIN_DATA, results.getResults().toString());
//                        ErrorCode = 1;
//                    } else {
//                        errorMessage = results.getErrorMessage();
//                        ErrorCode = 0;
//                    }
//
//
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    ErrorCode = 0;
//                }
//
//            }
//
//            return ErrorCode;
//        }
//
//
//        @Override
//        protected void onPostExecute(Integer result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            if (progressDialog.isShowing())
//                progressDialog.cancel();
//            if (result == 1) {
//
//                sharedPreferenceUtil.saveData(SharedPrefKeys.IS_VARIFIED, true);
//                Toast.makeText(LoginActivity.this, "User is Login", Toast.LENGTH_SHORT).show();
//                Intent mainIntent=new Intent(LoginActivity.this,MainActivity.class);
//
//                startActivity(mainIntent);
//                finish();
//            } else {
//                Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
//            }
//
//        }
//    }


}
