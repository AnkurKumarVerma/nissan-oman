package com.nissan.oman;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;


import com.google.gson.Gson;
import com.nissan.oman.alertMessage.AlertMessage;
import com.nissan.oman.interfaces.GetResponse;
import com.nissan.oman.model.Result;
import com.nissan.oman.sync.VolleyMethods;
import com.nissan.oman.sync.VolleyRequest;
import com.nissan.oman.url.ApiUrls;
import com.nissan.oman.utils.HelperMethods;
import com.nissan.oman.utils.SharedPrefKeys;
import com.nissan.oman.utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class SignUpActivity extends Activity implements GetResponse {


    private EditText editFirstName;
    private EditText editLastName;
    private TextView editdate;
    private EditText editmobile;
    private EditText editgmail;
    private EditText editPassword;
    private TextView btnSignUp;
    private TextView btnLogin;

    private HelperMethods helperMethod;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private AlertMessage alertMessage;


    private String editFirstNametxt = "";
    private String editLastNametxt = "";
    private String editdatetxt = "";
    private String editgmailtxt = "";
    private String editmobiletxt = "";
    private String editpasswordtxt = "";
    //private String editPhonetxt = "";

    private VolleyRequest volleyRequest;
    private VolleyMethods volleyMethods;


    private Context mContext;
    private ProgressDialog progressDialog;
    private DatePickerDialog datePickerDialog=null;
    private SimpleDateFormat dateFormatter=null;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        mContext = SignUpActivity.this;
        init();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }


    public void init() {
        // showHashKey(MainActivity.this);
        helperMethod = new HelperMethods();
        alertMessage = new AlertMessage(mContext);
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getApplicationContext());
        volleyRequest = VolleyRequest.getvolleyRequest();
        volleyMethods=new VolleyMethods();
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);


        editFirstName = (EditText) findViewById(R.id.editFirstName);
        editLastName = (EditText) findViewById(R.id.editLastName);
        editdate = (TextView) findViewById(R.id.editdate);
        editmobile = (EditText) findViewById(R.id.editmobile);
        editgmail = (EditText) findViewById(R.id.editgmail);
        editPassword = (EditText) findViewById(R.id.editPassword);

        btnLogin = (TextView) findViewById(R.id.btnLogin);
        btnSignUp = (TextView) findViewById(R.id.btnSignUp);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(mContext, LoginActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });



        editdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });


    datePicker();


    }


    public void datePicker()
    {
        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog=new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                Calendar newDate=Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                editdate.setText(dateFormatter.format(newDate.getTime()));


            }
        }, newCalendar
                .get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }




    private void showProgressBar() {
        progressDialog = ProgressDialog.show(mContext, "", "Please wait...", false);
        progressDialog.setCancelable(false);
    }

//    private void downloadData(String name,String email,String  password,String mobileno,String dob)
//    {
//        showProgressBar();
//        volleyRequest.initInterface(mContext);
//
//       // http://webisdomservices.in/nissanweb/webservices/users/?tag=register&name=seval&email=mayank@test.com&password=12345&mobile=7503066930&dob=10/10/1992
//
//
//        String url = ApiUrls.REGISTRATION + "&name=" + name + "&email=" + email+ "&password=" + password+ "&mobile=" + mobileno+ "&dob=" + dob;
//
//        volleyRequest.getResponseFromGetMethod(mContext, url, "register");
//
//
//    }


    public void signUp() {
        boolean isError = false;

        editFirstNametxt = editFirstName.getText().toString().trim();
        editLastNametxt = editLastName.getText().toString().trim();
        editdatetxt = editdate.getText().toString().trim();
        editmobiletxt = editmobile.getText().toString().trim();
        editgmailtxt = editgmail.getText().toString().trim();
        editpasswordtxt = editPassword.getText().toString().trim();


        if (editFirstNametxt.length() <= 0 || editLastNametxt.length() <= 0||
                editdatetxt.length() <= 0|| editmobiletxt.length() <= 0|| editgmailtxt.length() <= 0 || editpasswordtxt.length() <= 0) {
            isError = true;
            alertMessage.alertBox("Please enter all required fields", "Validation Error");
        } else if (!helperMethod.isPhoneNumberValid(editmobiletxt)) {
            isError = true;

            alertMessage.alertBox("Please enter valid phone number", "Error");
        } else if (!helperMethod.isValidEmail(editgmailtxt)) {
            isError = true;
            alertMessage.alertBox("Please enter valid gmail", "Error");

        }

        if (!isError) {
            showProgressBar();
            volleyMethods.downloadDataSignUp(volleyRequest,mContext,editFirstNametxt,editgmailtxt,editpasswordtxt,editmobiletxt,editdatetxt);
            //downloadData(editFirstNametxt,editgmailtxt,editpasswordtxt,editmobiletxt,editdatetxt);
           //webservice call
            // new LoginUserToCloud().execute();
        }

    }




    @Override
    public void getResponse(String error, String response) {

        if (progressDialog.isShowing())
            progressDialog.cancel();



        System.out.println(response);

        if (error == null) {

            Gson gson = new Gson();
            JSONObject jjjs;
            try {
                jjjs = new JSONObject(response);
                Result result = gson.fromJson(jjjs.toString(), Result.class);

                //				// Do something with object.

                if(result.getError()==0)
                {
                    sharedPreferenceUtil.saveData(SharedPrefKeys.USER_PROFILEDATA,(jjjs.toString()));
                    sharedPreferenceUtil.saveData(SharedPrefKeys.IS_VARIFIED,true);
                    Intent intent = new Intent(mContext,
                            MainActivity.class);
                    startActivity(intent);

                    finish();
                }
                else
                {
                   alertMessage.alertBox(result.getError_msg(),"Warning"); ;

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }



        } else {

                alertMessage.alertBox(error,"Warning");
            }

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }

    @Override
    public void onStop() {
        super.onStop();


    }


//
//    private class LoginUserToCloud extends AsyncTask<Void, Void, Integer> {
//
//        private ProgressDialog progressDialog;
//        private String errorMessage;
//        private JsonMethods jsonMethod;
//
//        @Override
//        protected void onPreExecute() {
//            // TODO Auto-generated method stub
//            super.onPreExecute();
//            progressDialog = ProgressDialog.show(mContext, "", "\nPlease wait...", false);
//            progressDialog.setCancelable(false);
//            jsonMethod=new JsonMethods();
//
//        }
//
//
//        @Override
//        protected Integer doInBackground(Void... params) {
//            // TODO Auto-generated method stub
//
//
//            int ErrorCode = 0;
//
//            if (helperMethod.isConnectedtoNetwork(mContext)) {
//
//
//
//                Login login = new Login();
//                login.setPhoneNumber(editPhonetxt);
//
//                login.setPassword(editPasswordtxt);
//
//
//
//                Gson gsonLogin = new GsonBuilder().create();
//
//                String convert = gsonLogin.toJson(login, Login.class);
//
//                String response = jsonMethod.accountLogin(convert);
//                sharedPreferenceUtil.setUsername("UserName",editUserNametxt);
//
//
//                Gson gson = new Gson();
//                JSONObject jsonResponse;
//                try {
//                    jsonResponse = new JSONObject(response);
//                    //sharedPreferenceUtil.saveData(SharedPrefKeys.LOGIN_DATA,jsonResponse.toString());
//                    ResultsLogin results = gson.fromJson(jsonResponse.toString(), ResultsLogin.class);
//
//                    if (results.getStatus().equalsIgnoreCase("OK")) {
//                        sharedPreferenceUtil.saveData(SharedPrefKeys.LOGIN_DATA, results.getResults().toString());
//                        ErrorCode = 1;
//                    } else {
//                        errorMessage = results.getErrorMessage();
//                        ErrorCode = 0;
//                    }
//
//
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    ErrorCode = 0;
//                }
//
//            }
//
//            return ErrorCode;
//        }
//
//
//        @Override
//        protected void onPostExecute(Integer result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            if (progressDialog.isShowing())
//                progressDialog.cancel();
//            if (result == 1) {
//
//                sharedPreferenceUtil.saveData(SharedPrefKeys.IS_VARIFIED, true);
//                Toast.makeText(LoginActivity.this, "User is Login", Toast.LENGTH_SHORT).show();
//                Intent mainIntent=new Intent(LoginActivity.this,MainActivity.class);
//
//                startActivity(mainIntent);
//                finish();
//            } else {
//                Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
//            }
//
//        }
//    }


}
