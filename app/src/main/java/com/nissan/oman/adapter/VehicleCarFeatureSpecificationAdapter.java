package com.nissan.oman.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.model.HomeItem;

import java.util.ArrayList;

/**
 * Created by jyoti on 5/3/16.
 */
public class VehicleCarFeatureSpecificationAdapter extends RecyclerView.Adapter<VehicleCarFeatureSpecificationAdapter.MyViewHolder>
{

    ArrayList<HomeItem> carFeatureItemArrayList;
    public VehicleCarFeatureSpecificationAdapter(ArrayList<HomeItem> carFeatureItemArrayList) {
        this.carFeatureItemArrayList = carFeatureItemArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.carfeature_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //HomeItem homeItem = homeList.get(position);

        holder.txtHeading.setText(carFeatureItemArrayList.get(position).getHeading());


    }

    @Override
    public int getItemCount() {
        return carFeatureItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtHeading;
        public ImageView imgNext;

        public MyViewHolder(View view) {
            super(view);

            txtHeading = (TextView) view.findViewById(R.id.txtHeading);
            imgNext = (ImageView) view.findViewById(R.id.imgNext);
            //imgNext.setVisibility(View.GONE);


        }
    }
}


