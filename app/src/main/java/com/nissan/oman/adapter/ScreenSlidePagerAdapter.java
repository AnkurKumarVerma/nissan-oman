 package com.nissan.oman.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nissan.oman.fragment.ScreenSlidePageFragment;


 public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter
{
	 private static final int NUM_PAGES = 2;
	 
	 public ScreenSlidePagerAdapter(FragmentManager fm)
     {
         super(fm);
     }

    /* public ScreenSlidePagerAdapter(android.app.FragmentManager fragmentManager) {
		// TODO Auto-generated constructor stub
	}*/

	@Override
     public Fragment getItem(int position) 
     {
		System.out.println("position" +position);
         return ScreenSlidePageFragment.create(position);
     }

     @Override
     public int getCount() {
         return NUM_PAGES;
     }

}
