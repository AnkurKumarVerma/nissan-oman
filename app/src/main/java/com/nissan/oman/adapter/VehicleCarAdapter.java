package com.nissan.oman.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.model.HomeItem;

import java.util.ArrayList;

/**
 * Created by jyoti on 5/2/16.
 */
public class VehicleCarAdapter extends RecyclerView.Adapter<VehicleCarAdapter.MyViewHolder>
{

   ArrayList<HomeItem> passengerCarItemArrayList;
      public VehicleCarAdapter(ArrayList<HomeItem> passengerCarItemArrayList) {
        this.passengerCarItemArrayList = passengerCarItemArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //HomeItem homeItem = homeList.get(position);
        holder.imgIcon.setImageResource(passengerCarItemArrayList.get(position).getIcon());
        holder.txtHeading.setText(passengerCarItemArrayList.get(position).getHeading());


    }

    @Override
    public int getItemCount() {
        return passengerCarItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgIcon;
         public TextView txtHeading;
        public ImageView imgNext;

        public MyViewHolder(View view) {
            super(view);
            imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            txtHeading = (TextView) view.findViewById(R.id.txtHeading);
            imgNext = (ImageView) view.findViewById(R.id.imgNext);

        }
    }
}
