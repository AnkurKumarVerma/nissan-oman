 package com.nissan.oman.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nissan.oman.fragment.ScreenSlidePageFragment;
import com.nissan.oman.fragment.ScreenSlidePageGalleryFragment;


 public class ScreenSlidePagerGalleryAdapter extends FragmentStatePagerAdapter
{
	 private static final int NUM_PAGES = 4;
    private String carName;

	 public ScreenSlidePagerGalleryAdapter(FragmentManager fm,String carName)
     {
         super(fm);
         this.carName=carName;
     }

    /* public ScreenSlidePagerAdapter(android.app.FragmentManager fragmentManager) {
		// TODO Auto-generated constructor stub
	}*/

	@Override
     public Fragment getItem(int position) 
     {
		System.out.println("position" +position);
         return ScreenSlidePageGalleryFragment.create(position,this.carName);
     }

     @Override
     public int getCount() {
         return NUM_PAGES;
     }

}
