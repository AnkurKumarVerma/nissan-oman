package com.nissan.oman.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.model.Performance;

import java.util.ArrayList;

/**
 * Created by jyoti on 5/2/16.
 */
public class VehicleCarFeatureSafetyAdapter extends RecyclerView.Adapter<VehicleCarFeatureSafetyAdapter.MyViewHolder>
{

   ArrayList<Performance> performanceItemArrayList;
      public VehicleCarFeatureSafetyAdapter(ArrayList<Performance> performanceItemArrayList) {
        this.performanceItemArrayList = performanceItemArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.performance_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //HomeItem homeItem = homeList.get(position);
        holder.txtSubHeading.setText(performanceItemArrayList.get(position).getSubHeading());
        holder.txtHeading.setText(performanceItemArrayList.get(position).getHeading());


    }

    @Override
    public int getItemCount() {
        return performanceItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

         public TextView txtHeading;
        public TextView txtSubHeading;


        public MyViewHolder(View view) {
            super(view);

            txtHeading = (TextView) view.findViewById(R.id.txtHeading);
            txtSubHeading = (TextView) view.findViewById(R.id.txtSubHeading);
            txtHeading.setVisibility(View.GONE);

        }
    }
}
