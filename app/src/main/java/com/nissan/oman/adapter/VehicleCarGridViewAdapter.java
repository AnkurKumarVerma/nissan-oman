package com.nissan.oman.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nissan.oman.R;
import com.nissan.oman.model.HomeItem;

import java.util.ArrayList;

/**
 * Created by jyoti on 5/3/16.
 */
public class VehicleCarGridViewAdapter extends RecyclerView.Adapter<VehicleCarGridViewAdapter.MyViewHolder>
{

    ArrayList<HomeItem> passengerCarItemArrayList;
    public VehicleCarGridViewAdapter(ArrayList<HomeItem> passengerCarItemArrayList) {
        this.passengerCarItemArrayList = passengerCarItemArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_grid_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //HomeItem homeItem = homeList.get(position);
        holder.imgIconGrid.setImageResource(passengerCarItemArrayList.get(position).getActualIconCar());



    }

    @Override
    public int getItemCount() {
        return passengerCarItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgIconGrid;

        public MyViewHolder(View view) {
            super(view);
            imgIconGrid = (ImageView) view.findViewById(R.id.imgIconGrid);


        }
    }
}


