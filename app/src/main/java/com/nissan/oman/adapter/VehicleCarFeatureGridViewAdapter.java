package com.nissan.oman.adapter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.model.HomeItem;

import java.util.ArrayList;

/**
 * Created by jyoti on 5/3/16.
 */
public class VehicleCarFeatureGridViewAdapter extends RecyclerView.Adapter<VehicleCarFeatureGridViewAdapter.MyViewHolder>
{

    ArrayList<HomeItem> carFeatureItemArrayList;
    private Context mContext;

    public VehicleCarFeatureGridViewAdapter(ArrayList<HomeItem> carFeatureItemArrayList, Context activity) {
        this.carFeatureItemArrayList = carFeatureItemArrayList;
        this.mContext = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.carfeature_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //HomeItem homeItem = homeList.get(position);

        holder.txtHeading.setText(carFeatureItemArrayList.get(position).getHeading());
        if(position==carFeatureItemArrayList.size()-1)
        {
           holder.imgNext.setVisibility(View.GONE);

            holder.llItemContainer.setBackground(new Drawable() {
                @Override
                public void draw(Canvas canvas) {
                    canvas.drawColor(mContext.getResources().getColor(R.color.btn_red));
                }

                @Override
                public void setAlpha(int alpha) {

                }

                @Override
                public void setColorFilter(ColorFilter cf) {

                }

                @Override
                public int getOpacity() {
                    return 0;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return carFeatureItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtHeading;
        public ImageView imgNext;
        LinearLayout llItemContainer;

        public MyViewHolder(View view) {
            super(view);

            txtHeading = (TextView) view.findViewById(R.id.txtHeading);
            imgNext = (ImageView) view.findViewById(R.id.imgNext);
            llItemContainer = (LinearLayout) view.findViewById(R.id.llItemContainer);



        }
    }
}


