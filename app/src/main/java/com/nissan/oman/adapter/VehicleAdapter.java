package com.nissan.oman.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.model.HomeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jyoti on 5/2/16.
 */
public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.MyViewHolder>
{


    ArrayList<HomeItem> vehicleList;
    private Context mContext;
    public VehicleAdapter(ArrayList<HomeItem> vehicleList, Context mContext) {
        this.vehicleList = vehicleList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //HomeItem homeItem = homeList.get(position);
        holder.imgIcon.setImageResource(vehicleList.get(position).getIcon());
        holder.txtHeading.setText(vehicleList.get(position).getHeading());


    }

    @Override
    public int getItemCount() {
        return vehicleList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgIcon;
         public TextView txtHeading;
        public ImageView imgNext;

        public MyViewHolder(View view) {
            super(view);
            imgIcon = (ImageView) view.findViewById(R.id.imgIcon);
            txtHeading = (TextView) view.findViewById(R.id.txtHeading);
            imgNext = (ImageView) view.findViewById(R.id.imgNext);

        }
    }
}
