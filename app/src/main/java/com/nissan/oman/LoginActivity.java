package com.nissan.oman;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.nissan.oman.alertMessage.AlertMessage;
import com.nissan.oman.interfaces.GetResponse;
import com.nissan.oman.model.Result;
import com.nissan.oman.sync.VolleyMethods;
import com.nissan.oman.sync.VolleyRequest;
import com.nissan.oman.utils.HelperMethods;
import com.nissan.oman.utils.SharedPrefKeys;
import com.nissan.oman.utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;


public class LoginActivity extends Activity implements GetResponse, View.OnTouchListener {


    private EditText editUserName;
    private EditText editPassword;
    private TextView txtForgetPassword;
    private TextView btnLogin;
    private TextView btnSignUp;
    private TextView btnFB;

    private VolleyRequest volleyRequest;
    private ProgressDialog progressDialog;
    private VolleyMethods volleyMethods;


    private HelperMethods helperMethod;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private AlertMessage alertMessage;


    private String editUserNametxt = "";
    private String editPasswordtxt = "";
    private CallbackManager callbackManager;

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        mContext = LoginActivity.this;
        init();
    }


    public void init() {
        // showHashKey(MainActivity.this);
        helperMethod = new HelperMethods();
        alertMessage = new AlertMessage(mContext);
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getApplicationContext());
        volleyRequest = VolleyRequest.getvolleyRequest();
        volleyMethods = new VolleyMethods();

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        editUserName = (EditText) findViewById(R.id.editUserName);
        editPassword = (EditText) findViewById(R.id.editPassword);

        editUserName.setOnTouchListener(this);
        editPassword.setOnTouchListener(this);

        btnSignUp = (TextView) findViewById(R.id.btnSignUp);
        btnLogin = (TextView) findViewById(R.id.btnLogin);

        btnFB = (TextView) findViewById(R.id.btnFB);
        txtForgetPassword = (TextView) findViewById(R.id.txtForgetPassword);


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(mainIntent);    //done
                finish();
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();                    //done
            }
        });


        txtForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send mail or something to send otp after that call   //done
                //enterotpscreen after that reset password call.

                Intent forgetIntent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(forgetIntent);    //done
                finish();

            }
        });
        //showHashKey(mContext);
        facebookLogin();


    }


//    private void showProgressBar() {
//        progressDialog = ProgressDialog.show(mContext, "", "Please wait...", false);
//        progressDialog.setCancelable(false);
//    }
//
//    private void downloadData(String email,String  password)
//    {
//        showProgressBar();
//        volleyRequest.initInterface(mContext);
//
//
//
//        String url = ApiUrls.LOGIN  + "&email=" + email+ "&password=" + password;
//
//        volleyRequest.getResponseFromGetMethod(mContext, url, "login");
//
//
//    }
//
//
//    private void downloadDataSignUp(String name,String email,String  password,String mobileno,String dob)
//    {
//        showProgressBar();
//        volleyRequest.initInterface(mContext);
//
//        // http://webisdomservices.in/nissanweb/webservices/users/?tag=register&name=seval&email=mayank@test.com&password=12345&mobile=7503066930&dob=10/10/1992
//
//
//        String url = ApiUrls.REGISTRATION + "&name=" + name + "&email=" + email+ "&password=" + password+ "&mobile=" + mobileno+ "&dob=" + dob;
//
//        volleyRequest.getResponseFromGetMethod(mContext, url, "register");
//
//
//    }


//    private void showHashKey(Context context) {
//        try {
//            PackageInfo info = context.getPackageManager().getPackageInfo("com.nissan.oman",
//                    PackageManager.GET_SIGNATURES);
//            for (android.content.pm.Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//
//                String sign = Base64.encodeToString(md.digest(), Base64.DEFAULT);
//                Log.e("KeyHash:", sign);
//                Toast.makeText(getApplicationContext(), sign, Toast.LENGTH_LONG).show();
//            }
//            Log.d("KeyHash:", "****------------***");
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//    }


    public void showProgressBar(Context mContext) {
        progressDialog = ProgressDialog.show(mContext, "", "Please wait...", false);
        progressDialog.setCancelable(false);
    }

    private void facebookLogin() {


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        try {

                                            JSONObject onj = new JSONObject(response.getJSONObject().toString());
                                            String user_id = onj.getString("id");
                                            String name = onj.getString("name");
                                            String email = onj.getString("email");

                                            showProgressBar(mContext);
                                            volleyMethods.downloadDataSignUp(volleyRequest, mContext, name, email, "", "", "");


                                            try {
//                                                model.setUrl((onj.getJSONObject("picture").getJSONObject("data").getString("url")));
//                                                sharePref.setImageUrl(model.getUrl());
                                            } catch (Exception e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    // Application code

                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,picture");
                        //  parameters.putString("fields", "id,email,gender,birthday,first_name,last_name,picture");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        // App code

//                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Googlelogin.this);
//
//                        alertBuilder.setTitle("Warning!");
//                        alertBuilder.setIcon(android.R.drawable.ic_menu_info_details);
//                        alertBuilder.setMessage("User cancelled Facebook login");
//
//                        alertBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//
//                            }
//                        });
//                        alertBuilder.show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code


//                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Googlelogin.this);
//
//                        alertBuilder.setTitle("Warning!");
//                        alertBuilder.setIcon(android.R.drawable.ic_menu_info_details);
//                        alertBuilder.setMessage("Error occurred. Try Again");
//
//                        alertBuilder.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//
//                            }
//                        });
//                        alertBuilder.show();
                    }
                });


        btnFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends", "email"));
                //LoginManager.getInstance().logInWithReadPermissions(mContext, Arrays.asList("public_profile", "user_friends", "email"));
            }
        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        callbackManager.onActivityResult(requestCode, resultCode, data);


    }


    @Override
    public void getResponse(String error, String response) {

        if (progressDialog.isShowing())
            progressDialog.cancel();


        System.out.println(response);

        if (error == null) {

            Gson gson = new Gson();
            JSONObject jjjs;
            try {
                jjjs = new JSONObject(response);
                Result result = gson.fromJson(jjjs.toString(), Result.class);

                //				// Do something with object.

                if (result.getError() == 0) {
                    sharedPreferenceUtil.saveData(SharedPrefKeys.USER_PROFILEDATA, (jjjs.toString()));
                    sharedPreferenceUtil.saveData(SharedPrefKeys.IS_VARIFIED, true);
                    Intent intent = new Intent(mContext,
                            MainActivity.class);
                    startActivity(intent);

                    finish();
                } else {
                    alertMessage.alertBox(result.getError_msg(), "Warning");
                    ;

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        } else {

            alertMessage.alertBox(error, "Warning");
        }

    }


    public void login() {
        boolean isError = false;

        editUserNametxt = editUserName.getText().toString().trim();
        editPasswordtxt = editPassword.getText().toString().trim();


        if (editUserNametxt.length() <= 0 || editPasswordtxt.length() <= 0) {
            isError = true;
            alertMessage.alertBox("Please enter all required fields", "Validation Error");
        } else if (!helperMethod.isPasswordValid(editPasswordtxt)) {
            isError = true;
            alertMessage.alertBox("Please enter valid password number", "Error");

        }

        if (!isError) {
            showProgressBar(mContext);
            volleyMethods.downloadDataLogin(mContext, editUserNametxt, editPasswordtxt, volleyRequest);


        }

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(event.getAction()==MotionEvent.ACTION_UP)
        {
            if(v instanceof EditText)
            {

                if(v.getId()==R.id.editUserName)
                {
                    editUserName.setHint("");

                    if(editPassword.getText().toString().trim().length()==0)
                    {
                        editPassword.setText("");
                        editPassword.setHint(getResources().getString(R.string.editpassword));
                    }
                }
                else if(v.getId()==R.id.editPassword)
                {
                    editPassword.setHint("");

                    if(editUserName.getText().toString().trim().length()==0)
                    {
                        editUserName.setText("");
                        editUserName.setHint(getResources().getString(R.string.edituser_name));
                    }
                }

             /* String value = ((EditText) v).getText().toString();

                if(value.trim().length()==0)
                {
                    ((EditText) v).setText(" ");
                }*/
            }
        }

        return false;
    }
}
