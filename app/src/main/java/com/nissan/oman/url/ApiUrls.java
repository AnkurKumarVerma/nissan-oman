package com.nissan.oman.url;

public class ApiUrls {



   // http://webisdomservices.in/nissanweb/webservices/users/?tag=register&name=seval&email=mayank@test.com&password=12345&mobile=7503066930&dob=10/10/1992

    public static final String REGISTRATION= APPConstants.NISSAN_HOST_NAME+APPConstants.USER_PROFILE+"?tag=register";


   // http://webisdomservices.in/nissanweb/webservices/users/?tag=login&email=maynk@test.com&password=12345

    public static final String LOGIN= APPConstants.NISSAN_HOST_NAME+APPConstants.USER_PROFILE+"?tag=login";


    public static final String REQUESTBROCHURE= APPConstants.NISSAN_HOST_NAME+APPConstants.REQUEST_BROCHURE+"?tag=request_a_brochure";

    public static final String FORGETPASSWORD= APPConstants.NISSAN_HOST_NAME+APPConstants.USER_PROFILE+"?tag=forget_password";
    public static final String REQUESTVEHICLE= APPConstants.NISSAN_HOST_NAME+APPConstants.GET_VEHICLE+"?tag=view";

}
