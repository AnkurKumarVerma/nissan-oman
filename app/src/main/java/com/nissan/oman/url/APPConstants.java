package com.nissan.oman.url;

public class APPConstants {


    public final static String NISSAN_HOST_NAME = "http://www.webisdomsolutions.com/nissanweb/webservices/";

    public final static String USER_PROFILE = "users/";

    public final static String REQUEST_BROCHURE= "request_brochure/";
    public final static String GET_VEHICLE= "vehicle/";


}
