package com.nissan.oman.alertMessage;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Created by jyoti on 11/5/15.
 */
public class AlertMessage {

    private Context mContext;

    public AlertMessage(Context mContext) {
        this.mContext = mContext;
    }

    public void alertBox(String message, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //finish();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

}
