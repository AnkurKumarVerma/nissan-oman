package com.nissan.oman;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nissan.oman.alertMessage.AlertMessage;
import com.nissan.oman.interfaces.GetResponse;
import com.nissan.oman.model.Result;
import com.nissan.oman.sync.VolleyMethods;
import com.nissan.oman.sync.VolleyRequest;
import com.nissan.oman.utils.HelperMethods;
import com.nissan.oman.utils.SharedPrefKeys;
import com.nissan.oman.utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;


public class ForgetPasswordActivity extends Activity implements GetResponse {


    private EditText editMobileNumber;


    private TextView btnBackLogin;
    private TextView btnSubmit;

    private VolleyRequest volleyRequest;
    private ProgressDialog progressDialog;
    private VolleyMethods volleyMethods;


    private HelperMethods helperMethod;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private AlertMessage alertMessage;


    private String editMobileNumbertxt = "";


    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgetpassword);
        mContext = ForgetPasswordActivity.this;
        init();
    }


    public void showProgressBar(Context mContext) {
        progressDialog = ProgressDialog.show(mContext, "", "Please wait...", false);
        progressDialog.setCancelable(false);
    }



    public void init() {
        // showHashKey(MainActivity.this);
        helperMethod = new HelperMethods();
        alertMessage = new AlertMessage(mContext);
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getApplicationContext());

        volleyRequest = VolleyRequest.getvolleyRequest();
        volleyMethods = new VolleyMethods();


        editMobileNumber = (EditText) findViewById(R.id.editMobileNumber);
        btnBackLogin = (TextView) findViewById(R.id.btnBackLogin);


        btnSubmit = (TextView) findViewById(R.id.btnSubmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            submit();
            }
        });



        btnBackLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


    }


    public void submit() {
        boolean isError = false;

        editMobileNumbertxt = editMobileNumber.getText().toString().trim();
       if(editMobileNumbertxt.length() <= 0) {
            isError = true;
            alertMessage.alertBox("Please enter all required fields", "Validation Error");
        } else if (!helperMethod.isValidEmail(editMobileNumbertxt)) {
            isError = true;

            alertMessage.alertBox("Please enter valid phone number", "Error");
        }

        if (!isError) {
            showProgressBar(mContext);
            volleyMethods.downloadDataForgetPassword(mContext,editMobileNumbertxt, volleyRequest);;
            // new LoginUserToCloud().execute();
        }

    }



    @Override
    public void getResponse(String error, String response) {

        if (progressDialog.isShowing())
            progressDialog.cancel();


        System.out.println(response);

        if (error == null) {

            Gson gson = new Gson();
            JSONObject jjjs;
            try {
                jjjs = new JSONObject(response);
                Result result = gson.fromJson(jjjs.toString(), Result.class);

                //				// Do something with object.

                if (result.getError() == 0) {

                    Intent intent = new Intent(mContext,
                            EnterOTPActivity.class);
                    startActivity(intent);

                    finish();
                } else {
                    alertMessage.alertBox(result.getError_msg(), "Warning");
                    ;

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        } else {

            alertMessage.alertBox(error, "Warning");
        }

    }



//
//    private class LoginUserToCloud extends AsyncTask<Void, Void, Integer> {
//
//        private ProgressDialog progressDialog;
//        private String errorMessage;
//        private JsonMethods jsonMethod;
//
//        @Override
//        protected void onPreExecute() {
//            // TODO Auto-generated method stub
//            super.onPreExecute();
//            progressDialog = ProgressDialog.show(mContext, "", "\nPlease wait...", false);
//            progressDialog.setCancelable(false);
//            jsonMethod=new JsonMethods();
//
//        }
//
//
//        @Override
//        protected Integer doInBackground(Void... params) {
//            // TODO Auto-generated method stub
//
//
//            int ErrorCode = 0;
//
//            if (helperMethod.isConnectedtoNetwork(mContext)) {
//
//
//
//                Login login = new Login();
//                login.setPhoneNumber(editPhonetxt);
//
//                login.setPassword(editPasswordtxt);
//
//
//
//                Gson gsonLogin = new GsonBuilder().create();
//
//                String convert = gsonLogin.toJson(login, Login.class);
//
//                String response = jsonMethod.accountLogin(convert);
//                sharedPreferenceUtil.setUsername("UserName",editUserNametxt);
//
//
//                Gson gson = new Gson();
//                JSONObject jsonResponse;
//                try {
//                    jsonResponse = new JSONObject(response);
//                    //sharedPreferenceUtil.saveData(SharedPrefKeys.LOGIN_DATA,jsonResponse.toString());
//                    ResultsLogin results = gson.fromJson(jsonResponse.toString(), ResultsLogin.class);
//
//                    if (results.getStatus().equalsIgnoreCase("OK")) {
//                        sharedPreferenceUtil.saveData(SharedPrefKeys.LOGIN_DATA, results.getResults().toString());
//                        ErrorCode = 1;
//                    } else {
//                        errorMessage = results.getErrorMessage();
//                        ErrorCode = 0;
//                    }
//
//
//                } catch (JSONException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                    ErrorCode = 0;
//                }
//
//            }
//
//            return ErrorCode;
//        }
//
//
//        @Override
//        protected void onPostExecute(Integer result) {
//            // TODO Auto-generated method stub
//            super.onPostExecute(result);
//            if (progressDialog.isShowing())
//                progressDialog.cancel();
//            if (result == 1) {
//
//                sharedPreferenceUtil.saveData(SharedPrefKeys.IS_VARIFIED, true);
//                Toast.makeText(LoginActivity.this, "User is Login", Toast.LENGTH_SHORT).show();
//                Intent mainIntent=new Intent(LoginActivity.this,MainActivity.class);
//
//                startActivity(mainIntent);
//                finish();
//            } else {
//                Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
//            }
//
//        }
//    }


}
