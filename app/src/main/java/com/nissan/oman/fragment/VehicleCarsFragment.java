package com.nissan.oman.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nissan.oman.R;

import com.nissan.oman.adapter.VehicleCarAdapter;
import com.nissan.oman.adapter.VehicleCarGridViewAdapter;
import com.nissan.oman.model.HomeItem;
import com.nissan.oman.touchEvent.ClickListenerInterface;
import com.nissan.oman.touchEvent.RecycleviewTouch;
import com.nissan.oman.utils.GridSpacingItemDecoration;


import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleCarsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    private VehicleCarAdapter vehicleCarAdapter;
      private VehicleCarGridViewAdapter vehicleCarGridViewAdapter;

    private RecyclerView recyclerView;

    private  int iconArray[]={R.drawable.passenger_icon,R.drawable.crossover_icon,R.drawable.suv_icon,R.drawable.lcv_icon};
    private String headingArray[]={"MICRA","SUNNY","TIIDA","SENTRA","ALTIMA","MAXIMA","37OZ","GTR"};
    private  int actualCarArray[]={R.drawable.micra,R.drawable.sunny,R.drawable.tiida,R.drawable.sentra,R.drawable.altima,R.drawable.maxima,R.drawable.threeseven,R.drawable.gtr};


    private String crossheadingArray[]={"JUKE","XTRAIL","PATHFINDER","MURANO"};
    private  int crossactualCarArray[]={R.drawable.juke,R.drawable.xtrail,R.drawable.pathfinder,R.drawable.murano};


    private String suvheadingArray[]={"XTERRA","ARMADA","PATROL"};
    private  int suvactualCarArray[]={R.drawable.xterra,R.drawable.armada,R.drawable.patrol};


    private String lcvheadingArray[]={"PICKUP","NAVARA","NV350 URVAN"};
    private  int lcvactualCarArray[]={R.drawable.pickup,R.drawable.navara,R.drawable.nvurvan};


    private  ArrayList<HomeItem> passengerCarArrayList=new ArrayList<>();


    private ImageView iconListview;
    private ImageView iconGridview;

    private GridLayoutManager gridLayoutManagerVertical;


    public VehicleCarsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VehicleCarsFragment newInstance(String param1, String param2) {
        VehicleCarsFragment fragment = new VehicleCarsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_passenger, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_passenger);
        iconListview = (ImageView) view.findViewById(R.id.iconListview);
        iconGridview = (ImageView) view.findViewById(R.id.iconGridview);
        if(passengerCarArrayList.size()>0)
        {
            passengerCarArrayList.clear();;
        }

        if(mParam1.equalsIgnoreCase("0")) {
        for(int i=0;i<actualCarArray.length;i++)
        {
             HomeItem passengerCarItem = new HomeItem();
                passengerCarItem.setIcon(iconArray[0]);
                passengerCarItem.setHeading(headingArray[i]);
                passengerCarItem.setActualIconCar(actualCarArray[i]);
                passengerCarArrayList.add(passengerCarItem);
            }

        }

        if(mParam1.equalsIgnoreCase("1")) {
            for(int i=0;i<crossactualCarArray.length;i++)
            {
                HomeItem passengerCarItem = new HomeItem();
                passengerCarItem.setIcon(iconArray[1]);
                passengerCarItem.setHeading(crossheadingArray[i]);
                passengerCarItem.setActualIconCar(crossactualCarArray[i]);
                passengerCarArrayList.add(passengerCarItem);
            }

        }

        if(mParam1.equalsIgnoreCase("2")) {
            for(int i=0;i<suvactualCarArray.length;i++)
            {
                HomeItem passengerCarItem = new HomeItem();
                passengerCarItem.setIcon(iconArray[2]);
                passengerCarItem.setHeading(suvheadingArray[i]);
                passengerCarItem.setActualIconCar(suvactualCarArray[i]);
                passengerCarArrayList.add(passengerCarItem);
            }

        }

        if(mParam1.equalsIgnoreCase("3")) {
            for(int i=0;i<lcvactualCarArray.length;i++)
            {
                HomeItem passengerCarItem = new HomeItem();
                passengerCarItem.setIcon(iconArray[3]);
                passengerCarItem.setHeading(lcvheadingArray[i]);
                passengerCarItem.setActualIconCar(lcvactualCarArray[i]);
                passengerCarArrayList.add(passengerCarItem);
            }

        }



        vehicleCarAdapter = new VehicleCarAdapter(passengerCarArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(vehicleCarAdapter);
        int spanCount = 3;
        int spacing = 2;
        boolean includeEdge = true;
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));









        iconListview.setOnClickListener(new View.OnClickListener() {
                                            @Override
              public void onClick(View view)
                  {

                     // view.setBackgroundColor(getResources().getColor(R.color.btn_red));
                      RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                      recyclerView.setLayoutManager(mLayoutManager);
                      recyclerView.setAdapter(vehicleCarAdapter);

                     }
                      });



        iconGridview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                vehicleCarGridViewAdapter = new VehicleCarGridViewAdapter(passengerCarArrayList);

                gridLayoutManagerVertical =
                        new GridLayoutManager(getActivity(),
                                3, //The number of Columns in the grid
                                LinearLayoutManager.VERTICAL,
                                false);






                recyclerView.setLayoutManager(gridLayoutManagerVertical);
                recyclerView.setAdapter(vehicleCarGridViewAdapter);
            }
        });
                //filldumptData();

                recyclerView.addOnItemTouchListener(new RecycleviewTouch(getActivity(), recyclerView, new ClickListenerInterface() {
                    @Override
                    public void onClick(View view, int position) {

                            VehicleCarFeatureFragment carFeatureFragment = VehicleCarFeatureFragment.newInstance(passengerCarArrayList.get(position).getActualIconCar(),passengerCarArrayList.get(position).getHeading(),position,mParam2);
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.realtabcontent, carFeatureFragment);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();



                        // Toast.makeText(getActivity(), position + " is selected!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
