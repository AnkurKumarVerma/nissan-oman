package com.nissan.oman.fragment;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nissan.oman.R;


import com.google.gson.Gson;
import com.nissan.oman.alertMessage.AlertMessage;
import com.nissan.oman.interfaces.GetResponse;
import com.nissan.oman.model.Result;
import com.nissan.oman.sync.VolleyMethods;
import com.nissan.oman.sync.VolleyRequest;
import com.nissan.oman.utils.HelperMethods;
import com.nissan.oman.utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleCarFeatureBrochureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeatureBrochureFragment extends Fragment implements GetResponse {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private String[] headingArray = {"MICRA", "SUNNY", "TIIDA", "SENTRA", "ALTIMA", "MAXIMA", "37OZ", "GTR"};
    private String[] locationArray = {"DELHI", "NOIDA", "GURGAON", "JAIPUR"};
    private GridLayoutManager gridLayoutManagerVertical;
    private int mParam1;
    private String mParam2;
    private String mParam4;
    private int mParam3;


    private AutoCompleteTextView editCarModel;
    private EditText editFirstName;
    private EditText editLastName;
    private AutoCompleteTextView editShowroom;
    private EditText editgmail;
    private EditText editphone;
    private RadioGroup radioGroup;
    private TextView btnSubmit;


    private String editFirstNametxt = "";
    private String editLastNametxt = "";
    private String editCarModeltxt = "";
    private String editgmailtxt = "";
    private String editmobiletxt = "";
    private String editlocationtxt = "";


    private HelperMethods helperMethod;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private AlertMessage alertMessage;


    private VolleyRequest volleyRequest;
    private VolleyMethods volleyMethods;
    private ProgressDialog progressDialog;


    public VehicleCarFeatureBrochureFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeatureBrochureFragment newInstance(String carName, String mParam2) {
        VehicleCarFeatureBrochureFragment fragment = new VehicleCarFeatureBrochureFragment();
        Bundle args = new Bundle();

        args.putString(ARG_PARAM2, carName);
        args.putString(ARG_PARAM4, mParam2);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mParam2 = getArguments().getString(ARG_PARAM2);

            mParam4 = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_requestbrochure, container, false);

        init(view);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });


        return view;
    }


    public void init(View view) {

        helperMethod = new HelperMethods();
        alertMessage = new AlertMessage(getActivity());
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getActivity());
        volleyRequest = VolleyRequest.getvolleyRequest();
        volleyMethods = new VolleyMethods();

        editCarModel = (AutoCompleteTextView) view.findViewById(R.id.editCarModel);
        editShowroom = (AutoCompleteTextView) view.findViewById(R.id.editShowroom);

        editCarModel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    ((AutoCompleteTextView)v).showDropDown();
                }

                return false;
            }
        });

        editShowroom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction()==MotionEvent.ACTION_UP)
                {
                    ((AutoCompleteTextView)v).showDropDown();
                }

                return false;
            }
        });

        editFirstName = (EditText) view.findViewById(R.id.editFirstName);
        editLastName = (EditText) view.findViewById(R.id.editLastName);

        editphone = (EditText) view.findViewById(R.id.editphone);
        editgmail = (EditText) view.findViewById(R.id.editgmail);

        btnSubmit = (TextView) view.findViewById(R.id.btnSubmit);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.select_dialog_singlechoice, headingArray);

        editCarModel.setThreshold(1);
        editCarModel.setAdapter(adapter);
//        editCarModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                editCarModeltxt = parent.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                editCarModeltxt = "";
//
//            }
//        });


        ArrayAdapter<String> locationAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.select_dialog_singlechoice, locationArray);

        editShowroom.setThreshold(1);

        editShowroom.setAdapter(locationAdapter);
//        editShowroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            public void onItemSelected(AdapterView<?> parent, View view,
//                                       int position, long id) {
//                editlocationtxt = parent.getItemAtPosition(position).toString();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                editlocationtxt = "";
//
//            }
//        });
    }


    private void showProgressBar() {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...", false);
        progressDialog.setCancelable(false);
    }


    public void submit() {
        boolean isError = false;

        editFirstNametxt = editFirstName.getText().toString().trim();
        editLastNametxt = editLastName.getText().toString().trim();
        editCarModeltxt = editCarModel.getText().toString().trim();
        editgmailtxt = editgmail.getText().toString().trim();
        editmobiletxt = editphone.getText().toString().trim();
        editlocationtxt = editShowroom.getText().toString().trim();


        if (editFirstNametxt.length() <= 0 || editLastNametxt.length() <= 0 ||
                editCarModeltxt.length() <= 0 || editmobiletxt.length() <= 0 || editgmailtxt.length() <= 0 || editlocationtxt.length() <= 0) {
            isError = true;
            alertMessage.alertBox("Please enter all required fields", "Validation Error");
        } else if (!helperMethod.isPhoneNumberValid(editmobiletxt)) {
            isError = true;

            alertMessage.alertBox("Please enter valid phone number", "Error");
        } else if (!helperMethod.isValidEmail(editgmailtxt)) {
            isError = true;
            alertMessage.alertBox("Please enter valid gmail", "Error");

        }

        if (!isError) {
            showProgressBar();
            volleyMethods.downloadDataReuestBrochure(volleyRequest,getActivity(), editCarModeltxt, editFirstNametxt, editLastNametxt, editlocationtxt, editgmailtxt, editmobiletxt, VehicleCarFeatureBrochureFragment.this);
            //downloadData(editFirstNametxt,editgmailtxt,editpasswordtxt,editmobiletxt,editdatetxt);
            //webservice call
            // new LoginUserToCloud().execute();
        }

    }


    @Override
    public void getResponse(String error, String response) {

        if (progressDialog.isShowing())
            progressDialog.cancel();


        System.out.println(response);

        if (error == null) {

            Gson gson = new Gson();
            JSONObject jjjs;
            try {
                jjjs = new JSONObject(response);
                Result result = gson.fromJson(jjjs.toString(), Result.class);

                //				// Do something with object.

                if (result.getError() == 0) {
                    Toast.makeText(getActivity(),"Your request for  brochure is successfully submitted",Toast.LENGTH_SHORT).show();
                    //alertMessage.alertBox("you got brochure successfully ", "Sucessful");
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = manager.beginTransaction();
                    trans.remove(VehicleCarFeatureBrochureFragment.this);
                    trans.commit();
                    manager.popBackStack();



                } else {
                    alertMessage.alertBox(result.getError_msg(), "Warning");
                    ;

                }


            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        } else {

            alertMessage.alertBox(error, "Warning");
        }

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        // mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
