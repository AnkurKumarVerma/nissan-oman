package com.nissan.oman.fragment;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nissan.oman.MainActivity;
import com.nissan.oman.R;
import com.nissan.oman.adapter.ExpandableListAdapter;
import com.nissan.oman.adapter.VehicleCarFeatureSpecificationAdapter;
import com.nissan.oman.model.HomeItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehicleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeatureSpecificationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;




    public VehicleCarFeatureSpecificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     *
     *
     */

    private TextView carName;
     private ExpandableListView expListView;
    private  ExpandableListAdapter listAdapter;
    private VehicleCarFeatureSpecificationAdapter vehicleCarFeatureSpecificationAdapter;
    private ArrayList<HomeItem> specificationItemArrayList=new ArrayList<>();
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;


    private String specificationheadingArray[]={"DISCLAIMER","ENGINE","TRANSMISSION","SUSPENSION","BRAKES","TYRES & WHEELS","DIMENSIONS & WEIGHT","CAPACITIES"};
    private String engineArray[]={"No of cylinder Configuration -1.6 litre gasoline engine",
            "valves per cylinder -4 in line",
            "Air intake system -Normal aspiration",
            "Engine displacement-1598"
            };

    private String transmission[]={"Torque converter with lock up"};

    private String suspension[]={"front"};

    private String brakes[]={"10.7"};
    private String dimension[]={"1207/1226(upper),1670 kg"};





    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeatureSpecificationFragment newInstance(int  resId, String carName) {
        VehicleCarFeatureSpecificationFragment fragment = new VehicleCarFeatureSpecificationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, resId);
        args.putString(ARG_PARAM2, carName);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_carfeatures_expand_specfication, container, false);

         carName = (TextView) view.findViewById(R.id.carName);
        //carFeatureName = (TextView) view.findViewById(R.id.carFeatureName);
        expListView = (ExpandableListView)view.findViewById(R.id.recycler_view_specfication);
        if(specificationItemArrayList.size()>0)
        {
            specificationItemArrayList.clear();
        }

        carName.setText("SPECIFICATION");


//        for(int i=0;i<specificationheadingArray.length;i++)
//        {
//            HomeItem item=new HomeItem();
//            item.setHeading(specificationheadingArray[i]);
//            specificationItemArrayList.add(item);
//
//        }
        if(mParam2.equalsIgnoreCase("JUKE")) {
            prepareListData();
        }
        else
        {
            prepareListDataXtrail();
        }
        //setGroupIndicatorToRight();
        listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        vehicleCarFeatureSpecificationAdapter = new VehicleCarFeatureSpecificationAdapter(specificationItemArrayList);

        expListView.setAdapter(listAdapter);

//        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
//
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Expanded",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//
//        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Collapsed",
//                        Toast.LENGTH_SHORT).show();
//
//            }
//        });










        return view;
    }

    private void setGroupIndicatorToRight() {
        /* Get the screen width */
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;

        expListView.setIndicatorBounds(width - getDipsFromPixel(16), width
                - getDipsFromPixel(16));


        Toast.makeText(getActivity(),
                "drawable_width: " + width +"\n" +
                        "expandableListView.getWidth()" + expListView.getWidth(),
                Toast.LENGTH_LONG).show();
    }

    // Convert pixel to dip
    public int getDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }



//    public void onWindowFocusChanged() {
//
//
//
//
//
//
//        Drawable drawable_groupIndicator =
//                getResources().getDrawable(R.drawable.custom_arrow1);
//        int drawable_width = drawable_groupIndicator.getMinimumWidth();
//
//        if(android.os.Build.VERSION.SDK_INT <
//                android.os.Build.VERSION_CODES.JELLY_BEAN_MR2){
//            expListView.setIndicatorBounds(
//                    expListView.getWidth()-drawable_width,
//                    expListView.getWidth());
//        }else{
//            expListView.setIndicatorBoundsRelative(
//                    expListView.getWidth()-drawable_width,
//                    expListView.getWidth());
//        }
//
//        Toast.makeText(getActivity(),
//                "drawable_width: " + drawable_width +"\n" +
//                        "expandableListView.getWidth()" + expListView.getWidth(),
//                Toast.LENGTH_LONG).show();
//    }





    private void prepareListDataXtrail() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();


        for(int i=0;i<specificationheadingArray.length;i++)
        {
            listDataHeader.add(specificationheadingArray[i]);

        }


        List<String> disclamer = new ArrayList<String>();
        disclamer.add("Specifications are subject to change with  out prior notification and vary for different countries depending on local market conditions.Please consult local dealer to ensure that the vehicles delivered accords with your");


        // Adding child data
        List<String> engine = new ArrayList<String>();
//        for(int i=0;i<engineArray.length;i++)
//        {
//            engine.add(engineArray[i]);
//
//        }


        List<String> transmissionArray = new ArrayList<String>();
//        for(int i=0;i<transmission.length;i++)
//        {
//            transmissionArray.add(transmission[i]);
//
//        }

        List<String> suspensionArray = new ArrayList<String>();
//        for(int i=0;i<suspension.length;i++)
//        {
//            suspensionArray.add(suspension[i]);
//
//        }


        List<String> brakesArray = new ArrayList<String>();
//        for(int i=0;i<brakes.length;i++)
//        {
//            brakesArray.add(brakes[i]);
//
//        }


        List<String> dimensionArray = new ArrayList<String>();
//        for(int i=0;i<dimension.length;i++)
//        {
//            dimensionArray.add(dimension[i]);
//
//        }

        List<String> TyreArray = new ArrayList<String>();

        List<String> CaacitiesArray = new ArrayList<String>();



        listDataChild.put(listDataHeader.get(0), disclamer);
        listDataChild.put(listDataHeader.get(1), engine); // Header, Child data
        listDataChild.put(listDataHeader.get(2), transmissionArray);
        listDataChild.put(listDataHeader.get(3), suspensionArray);
        listDataChild.put(listDataHeader.get(4), brakesArray); // Header, Child data
        listDataChild.put(listDataHeader.get(5),TyreArray);
        listDataChild.put(listDataHeader.get(6), dimensionArray);
        listDataChild.put(listDataHeader.get(7), CaacitiesArray);
    }





    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();


        for(int i=0;i<specificationheadingArray.length;i++)
        {
            listDataHeader.add(specificationheadingArray[i]);

        }

        // Adding child data
        List<String> disclamer = new ArrayList<String>();
        disclamer.add("Specifications are subject to change with  out prior notification and vary for different countries depending on local market conditions.Please consult local dealer to ensure that the vehicles delivered accords with your");


        List<String> engine = new ArrayList<String>();
        for(int i=0;i<engineArray.length;i++)
        {
            engine.add(engineArray[i]);

        }


        List<String> transmissionArray = new ArrayList<String>();
        for(int i=0;i<transmission.length;i++)
        {
            transmissionArray.add(transmission[i]);

        }

        List<String> suspensionArray = new ArrayList<String>();
        for(int i=0;i<suspension.length;i++)
        {
            suspensionArray.add(suspension[i]);

        }


        List<String> brakesArray = new ArrayList<String>();
        for(int i=0;i<brakes.length;i++)
        {
            brakesArray.add(brakes[i]);

        }


        List<String> dimensionArray = new ArrayList<String>();
        for(int i=0;i<dimension.length;i++)
        {
            dimensionArray.add(dimension[i]);

        }

        List<String> TyreArray = new ArrayList<String>();

        List<String> CaacitiesArray = new ArrayList<String>();


        listDataChild.put(listDataHeader.get(0), disclamer);
        listDataChild.put(listDataHeader.get(1), engine); // Header, Child data
        listDataChild.put(listDataHeader.get(2), transmissionArray);
        listDataChild.put(listDataHeader.get(3), suspensionArray);
        listDataChild.put(listDataHeader.get(4), brakesArray); // Header, Child data
        listDataChild.put(listDataHeader.get(5),TyreArray);
        listDataChild.put(listDataHeader.get(6), dimensionArray);
        listDataChild.put(listDataHeader.get(7), CaacitiesArray);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
