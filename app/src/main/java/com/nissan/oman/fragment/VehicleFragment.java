package com.nissan.oman.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.google.gson.Gson;
import com.nissan.oman.R;
import com.nissan.oman.adapter.HomeAdapter;

import com.nissan.oman.adapter.ScreenSlidePagerAdapter;
import com.nissan.oman.adapter.VehicleAdapter;
import com.nissan.oman.adapter.VehicleCarAdapter;
import com.nissan.oman.alertMessage.AlertMessage;
import com.nissan.oman.interfaces.GetResponse;
import com.nissan.oman.model.HomeItem;
import com.nissan.oman.model.Result;
import com.nissan.oman.sync.VolleyMethods;
import com.nissan.oman.sync.VolleyRequest;
import com.nissan.oman.touchEvent.ClickListenerInterface;
import com.nissan.oman.touchEvent.RecycleviewTouch;
import com.nissan.oman.utils.HelperMethods;
import com.nissan.oman.utils.SharedPrefKeys;
import com.nissan.oman.utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleFragment extends Fragment implements GetResponse {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    private VehicleAdapter vehicleAdapter;
    private RecyclerView recyclerView;

    private  int iconArray[]={R.drawable.passenger_icon,R.drawable.crossover_icon,R.drawable.suv_icon,R.drawable.lcv_icon};
    private String headingArray[]={"PASSENGER CARS","CROSSOVERS","SUVS","LCVs"};


    private ArrayList<HomeItem> vehicleList=new ArrayList<>();


    private HelperMethods helperMethod;
    private SharedPreferenceUtil sharedPreferenceUtil;
    private AlertMessage alertMessage;


    private VolleyRequest volleyRequest;
    private VolleyMethods volleyMethods;
    private ProgressDialog progressDialog;




    public VehicleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VehicleFragment newInstance(String param1, String param2) {
        VehicleFragment fragment = new VehicleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_vehicle, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_vehicle);

        helperMethod = new HelperMethods();
        alertMessage = new AlertMessage(getActivity());
        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getActivity());
        volleyRequest = VolleyRequest.getvolleyRequest();
        volleyMethods = new VolleyMethods();


//        showProgressBar();
//        volleyMethods.downloadDataReuestVehicle(volleyRequest,getActivity(),VehicleFragment.this);


        if(vehicleList.size()>0)
        {
            vehicleList.clear();
        }

        for(int i=0;i<iconArray.length;i++)
        {
            HomeItem homeItem=new HomeItem();
            homeItem.setIcon(iconArray[i]);
            homeItem.setHeading(headingArray[i]);
            vehicleList.add(homeItem);
        }



        vehicleAdapter = new VehicleAdapter(vehicleList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(vehicleAdapter);
        //filldumptData();

        recyclerView.addOnItemTouchListener(new RecycleviewTouch(getActivity(), recyclerView, new ClickListenerInterface() {
            @Override
            public void onClick(View view, int position) {



                   /* VehicleCarsFragment vehicleCarsFragment = VehicleCarsFragment.newInstance(position+"","");
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent,vehicleCarsFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();*/


                getFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.realtabcontent,
                        VehicleCarsFragment.newInstance(position+"",headingArray[position])).commit();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return view;
    }





    @Override
    public void getResponse(String error, String response) {

        if (progressDialog.isShowing())
            progressDialog.cancel();


        System.out.println(response);

        if (error == null) {

            Gson gson = new Gson();
//            JSONObject jjjs;
           try {
//                jjjs = new JSONObject(response);

                sharedPreferenceUtil.saveData(SharedPrefKeys.VEHICLE_DATA,(response.toString()));

//                Result result = gson.fromJson(jjjs.toString(), Result.class);

                //				// Do something with object.

//                if (result.getError() == 0) {
//
//
//                } else {
//                    alertMessage.alertBox(result.getError_msg(), "Warning");
//
//
//                }


            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        } else {

            alertMessage.alertBox(error, "Warning");
        }

    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void showProgressBar() {
        progressDialog = ProgressDialog.show(getActivity(), "", "Please wait...", false);
        progressDialog.setCancelable(false);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
