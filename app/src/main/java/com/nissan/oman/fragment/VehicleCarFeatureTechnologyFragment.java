package com.nissan.oman.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.adapter.VehicleCarFeaturePerformanceAdapter;
import com.nissan.oman.adapter.VehicleCarFeatureSafetyAdapter;
import com.nissan.oman.model.Performance;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehicleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeatureTechnologyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;




    public VehicleCarFeatureTechnologyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     *
     *
     */




    private TextView carName,carFeatureName;
    private RecyclerView recycler_view_performance;
    private VehicleCarFeaturePerformanceAdapter vehicleCarFeaturePerformanceAdapter;
    private VehicleCarFeatureSafetyAdapter vehicleCarFeatureSafetyAdapter;
    private ArrayList<Performance> performanceCarArrayList=new ArrayList<>();



    public String subheading1="New Nissan integrated control system : "+System.getProperty("line.separator")+"• Three driving modes (SPORT/NORMAL/ECO) to play with "+System.getProperty("line.separator")+"• User friendly interface with LED to manage both air conditioning and driving mode selection "+System.getProperty("line.separator")+"• Auxilary audio input to play your iPod.";
    public String subheading2="Never fumble with keys again. Just leave the innovative Nissan Intelligent Key® in your pocket or bag. As you walk up to the vehicle, it senses the key and allows you to unlock the door with a tap of a button on the door handle. Once inside, use the Push Button Ignition to start the engine.";

    public String heading1="The new Nissan Juke offers the very latest fully integrated technology in car settings.";
    public String heading2="Forget About Your Keys\u2028";


   // public String subheadingXtrail="New Nissan integrated control system : "+System.getProperty("line.separator")+"• Three driving modes (SPORT/NORMAL/ECO) to play with "+System.getProperty("line.separator")+"• User friendly interface with LED to manage both air conditioning and driving mode selection "+System.getProperty("line.separator")+"• Auxilary audio input to play your iPod.";
    public String subheadingXtrail1="Technology changes everything. Loaded with available high tech and instant entertainment features, X-Trail transforms your whole drive.";
    public String subheadingXtrail2="Intelligent Key let’s you lock and unlock the doors and litigate, start the engine and drive away — all without removing it from your pocket.";

    public String subheadingXtrail3="No fumbling for a key. Just a simple touch, and away you go.";
    public String subheadingXtrail4="The Advanced Drive-Assist Display serves up info right in front of you – helping to minimize time looking away.";




    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeatureTechnologyFragment newInstance(int  resId, String carName) {
        VehicleCarFeatureTechnologyFragment fragment = new VehicleCarFeatureTechnologyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, resId);
        args.putString(ARG_PARAM2, carName);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_carfeatures_performance, container, false);

         carName = (TextView) view.findViewById(R.id.carName);
        carFeatureName = (TextView) view.findViewById(R.id.carFeatureName);
        recycler_view_performance = (RecyclerView) view.findViewById(R.id.recycler_view_performance);

        if(performanceCarArrayList.size()>0)
        {
            performanceCarArrayList.clear();;
        }

        carName.setText(mParam2);
        carFeatureName.setText("TECHNOLOGY");

        if(mParam2.equalsIgnoreCase("JUKE")) {


            Performance performance = new Performance();
            performance.setHeading(heading1);
            performance.setSubHeading(subheading1);
            performanceCarArrayList.add(performance);

            Performance performance1 = new Performance();
            performance1.setHeading(heading2);
            performance1.setSubHeading(subheading2);
            performanceCarArrayList.add(performance1);



            vehicleCarFeaturePerformanceAdapter = new VehicleCarFeaturePerformanceAdapter(performanceCarArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recycler_view_performance.setLayoutManager(mLayoutManager);
            recycler_view_performance.setItemAnimator(new DefaultItemAnimator());
            recycler_view_performance.setAdapter(vehicleCarFeaturePerformanceAdapter);
        }
        else
        {
            Performance performance1 = new Performance();
            //performance1.setHeading(headingxtrail1);
            performance1.setSubHeading(subheadingXtrail1);
            performanceCarArrayList.add(performance1);

            Performance performance2 = new Performance();
            //performance2.setHeading(headingxtrail2);
            performance2.setSubHeading(subheadingXtrail2);
            performanceCarArrayList.add(performance2);

            Performance performance3 = new Performance();
            //performance3.setHeading(headingxtrail3);
            performance3.setSubHeading(subheadingXtrail3);
            performanceCarArrayList.add(performance3);

            Performance performance4 = new Performance();
            //performance4.setHeading(headingxtrail4);
            performance4.setSubHeading(subheadingXtrail4);
            performanceCarArrayList.add(performance4);


            vehicleCarFeatureSafetyAdapter = new VehicleCarFeatureSafetyAdapter(performanceCarArrayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recycler_view_performance.setLayoutManager(mLayoutManager);
            recycler_view_performance.setItemAnimator(new DefaultItemAnimator());
            recycler_view_performance.setAdapter(vehicleCarFeatureSafetyAdapter);
        }













        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
