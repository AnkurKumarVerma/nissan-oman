/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nissan.oman.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nissan.oman.R;


public class ScreenSlidePageGalleryFragment extends Fragment {

    public static final String ARG_PAGE = "page";
    public static final String ARG_PAGE1 = "carname";


    private int mPageNumber;
    private String carName;



    public static ScreenSlidePageGalleryFragment create(int pageNumber,String carName) {
    	ScreenSlidePageGalleryFragment fragment = new ScreenSlidePageGalleryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        args.putString(ARG_PAGE1, carName);
        fragment.setArguments(args);
        System.out.println("in screen slide" +ARG_PAGE);
        return fragment;
    }

    public ScreenSlidePageGalleryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
        carName = getArguments().getString(ARG_PAGE1);

        System.out.println("pageNumber" +mPageNumber);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        final ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_screen_slide_page, container, false);
        
       
        ImageView image=(ImageView)rootView.findViewById(R.id.image1);

        System.out.println("pageNumber in screen slide page fragment" +mPageNumber);
if(carName.equalsIgnoreCase("JUKE")) {
    if (mPageNumber == 0) {
        image.setImageResource(R.drawable.juke1);
    } else if (mPageNumber == 1) {
        image.setImageResource(R.drawable.juke2);
    } else if (mPageNumber == 2) {
        image.setImageResource(R.drawable.juke3);
    } else if (mPageNumber == 3) {
        image.setImageResource(R.drawable.juke4);
    }
}
        else
{
    if (mPageNumber == 0) {
        image.setImageResource(R.drawable.xtrail1);
    } else if (mPageNumber == 1) {
        image.setImageResource(R.drawable.xtrail2);
    } else if (mPageNumber == 2) {
        image.setImageResource(R.drawable.xtrail3);
    } else if (mPageNumber == 3) {
        image.setImageResource(R.drawable.xtrail4);
    }
}


    

        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
    
    
    

	
}
