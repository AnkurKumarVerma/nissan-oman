package com.nissan.oman.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.adapter.VehicleCarFeaturePerformanceAdapter;
import com.nissan.oman.adapter.VehicleCarFeatureSafetyAdapter;
import com.nissan.oman.model.Performance;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehicleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeatureSafetyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;




    public VehicleCarFeatureSafetyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     *
     *
     */




    private TextView carName,carFeatureName;
    private RecyclerView recycler_view_performance;
    private VehicleCarFeatureSafetyAdapter vehicleCarFeatureSafetyAdapter;
    private ArrayList<Performance> performanceCarArrayList=new ArrayList<>();



    public String subheading1="Nissan’s concept of “the vehicle that helps protect people.” In the area of safety technology, Nissan pursues innovation as part of its “Safety Shield” concept, an advanced, proactive approach to safety issues based on the idea that cars should help protect people. This approach provides various measures to help the driver and passengers better avoid dangers in ways that are optimized to each of a wide range of circumstances that the vehicle may be in, from “risk has not yet appeared” to “post-crash”.";
    public String subheading2="•Six standard air bags wrap you in safety. Two dual-stage supplemental front and side-impact air bags complement roof-mounted, side-impact air bags which work to keep both front and rear passengers safe.[2] Front-seat Active Head Restraints help reduce the chance of whiplash injuries to you and your front seat passenger by moving up and forward during certain rear-end collisions. [1] with Leather Package [2] Air bags are only a supplemental restraint system; always wear your seat belt. Even with the occupant-classification sensor, rear-facing child restraints should not be placed in the front-passenger’s seat. Also, all children 12 and under should ride in the rear seat properly secured in child restraints, booster";



    public String subheadingxtrail1="•Nissan advanced safety technologies help to provide a more confident drive by keeping you informed of potential risks, with auditory and/or visual warning, The group of features include Blind Spot Warning., Lane Departure Warning, and Moving Object Departure Warning, and Moving Object Detection, which detects movement around your vehicle when you are parking.";
    public String subheadingxtrail2="•Dual front SRS airbags in the Nissan X-TRAIL are inflated in milliseconds and work with the seat belts to protect occupants during a frontal impact.";
    public String subheadingxtrail3="•A slick spot in a corner can suddenly have you going in a direction you didn’t want. Stability control monitors your steering and braking to help you maintain your steered path under certain conditions by reducing engine output and/or applying brake pressure to specific wheels.";



    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeatureSafetyFragment newInstance(int  resId, String carName) {
        VehicleCarFeatureSafetyFragment fragment = new VehicleCarFeatureSafetyFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, resId);
        args.putString(ARG_PARAM2, carName);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_carfeatures_performance, container, false);

         carName = (TextView) view.findViewById(R.id.carName);
        carFeatureName = (TextView) view.findViewById(R.id.carFeatureName);
        recycler_view_performance = (RecyclerView) view.findViewById(R.id.recycler_view_performance);

        if(performanceCarArrayList.size()>0)
        {
            performanceCarArrayList.clear();;
        }

        carName.setText(mParam2);
        carFeatureName.setText("SAFETY");

        if(mParam2.equalsIgnoreCase("JUKE")) {
            Performance performance = new Performance();
            performance.setHeading("");
            performance.setSubHeading(subheading1);
            performanceCarArrayList.add(performance);

            Performance performance1 = new Performance();
            performance1.setHeading("");
            performance1.setSubHeading(subheading2);
            performanceCarArrayList.add(performance1);
        }
        else
        {
            Performance performance = new Performance();
            performance.setHeading("");
            performance.setSubHeading(subheadingxtrail1);
            performanceCarArrayList.add(performance);

            Performance performance1 = new Performance();
            performance1.setHeading("");
            performance1.setSubHeading(subheadingxtrail2);
            performanceCarArrayList.add(performance1);

            Performance performance2 = new Performance();
            performance2.setHeading("");
            performance2.setSubHeading(subheadingxtrail3);
            performanceCarArrayList.add(performance2);


        }



        vehicleCarFeatureSafetyAdapter = new VehicleCarFeatureSafetyAdapter(performanceCarArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view_performance.setLayoutManager(mLayoutManager);
        recycler_view_performance.setItemAnimator(new DefaultItemAnimator());
        recycler_view_performance.setAdapter(vehicleCarFeatureSafetyAdapter);




        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
