package com.nissan.oman.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.adapter.VehicleCarFeaturePerformanceAdapter;
import com.nissan.oman.model.Performance;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehicleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeatureVersatilityFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;




    public VehicleCarFeatureVersatilityFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     *
     *
     */

    private TextView carName,carFeatureName;
    private RecyclerView recycler_view_performance;
    private VehicleCarFeaturePerformanceAdapter vehicleCarFeaturePerformanceAdapter;
    private ArrayList<Performance> performanceCarArrayList=new ArrayList<>();




    public String  headingJuke1="Compact size. Big confidence.";
    public String subheadingJuke1="Smart. Bold. Compact. This all-new sport cross is like no other Nissan. JUKE’s agile size means better fuel efficiency, not to mention better parking efficiency. And JUKE stands tall with a ground clearance that offers a commanding view of the road and safeguards against scuffs.";

    public String  headingJuke2="All Access";
    public String subheadingJuke2= "Loading and unloading your gear couldn’t be easier. Just pop JUKE’s hatchback to reveal a customizable cargo space with enough room for all your essentials. Plus, its natural, curved shape enhances JUKE’s overall aerodynamic look and feel.";

    public String  headingJuke3="Clever Storage";
    public String subheadingJuke3= "Check under the rear cargo area floor and you’ll find a secret storage stash. It’s just the right size to store your roadside safety equipment or anything you want to keep out of sight.";

    public String  headingJuke4="Space to Fit Your Lifestyle";
    public String subheadingJuke4= "With up to 830L loading space, there’s more than enough room to fit your DJ gear, sports equipment or just about anything else.";


    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeatureVersatilityFragment newInstance(int  resId, String carName) {
        VehicleCarFeatureVersatilityFragment fragment = new VehicleCarFeatureVersatilityFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, resId);
        args.putString(ARG_PARAM2, carName);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_carfeatures_performance, container, false);

         carName = (TextView) view.findViewById(R.id.carName);
        carFeatureName = (TextView) view.findViewById(R.id.carFeatureName);
        recycler_view_performance = (RecyclerView) view.findViewById(R.id.recycler_view_performance);

        if(performanceCarArrayList.size()>0)
        {
            performanceCarArrayList.clear();;
        }

        carName.setText(mParam2);
        carFeatureName.setText("VERSATILITY");


        Performance performance1=new Performance();
            performance1.setHeading(headingJuke1);
            performance1.setSubHeading(subheadingJuke1);
            performanceCarArrayList.add(performance1);

        Performance performance2=new Performance();
        performance2.setHeading(headingJuke2);
        performance2.setSubHeading(subheadingJuke2);
        performanceCarArrayList.add(performance2);


        Performance performance3=new Performance();
        performance3.setHeading(headingJuke3);
        performance3.setSubHeading(subheadingJuke3);
        performanceCarArrayList.add(performance3);

        Performance performance4=new Performance();
        performance4.setHeading(headingJuke4);
        performance4.setSubHeading(subheadingJuke4);
        performanceCarArrayList.add(performance4);


        vehicleCarFeaturePerformanceAdapter = new VehicleCarFeaturePerformanceAdapter(performanceCarArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view_performance.setLayoutManager(mLayoutManager);
        recycler_view_performance.setItemAnimator(new DefaultItemAnimator());
        recycler_view_performance.setAdapter(vehicleCarFeaturePerformanceAdapter);









        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
