package com.nissan.oman.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nissan.oman.R;
import com.nissan.oman.adapter.VehicleCarAdapter;
import com.nissan.oman.adapter.VehicleCarFeaturePerformanceAdapter;
import com.nissan.oman.adapter.VehicleCarFeatureSafetyAdapter;
import com.nissan.oman.model.HomeItem;
import com.nissan.oman.model.Performance;
import com.nissan.oman.utils.GridSpacingItemDecoration;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehicleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeaturePerformanceFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;




    public VehicleCarFeaturePerformanceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     *
     *
     */

    private TextView carName,carFeatureName;
    private RecyclerView recycler_view_performance;
    private VehicleCarFeaturePerformanceAdapter vehicleCarFeaturePerformanceAdapter;
    private VehicleCarFeatureSafetyAdapter vehicleCarFeatureSafetyAdapter;
    private ArrayList<Performance> performanceCarArrayList=new ArrayList<>();




    public String  headingJuke1="Adapt At Will";
    public String subheadingJuke1="Looking for a shot of adrenaline? With the touch of a button, kick your engine into sport mode using the available Integrated Control (I-CON) system. Or dial down to Eco mode and dodge the pump a little longer. You decide. JUKE obeys.";

    public String  headingJuke2="Speed-sensitive Steering";
    public String subheadingJuke2= "Using Electric Power Steering instead of hydraulic power steering cuts down on the amount of power needed from the engine. That means more miles out of every tank. Plus, it automatically adjusts steering feel based on vehicle speed.";



    public String  headingxtrail1="Engine";
    public String subheadingxtrail1="The Nissan X-TRAIL delivers so much power we even named the engine after the most powerful thing we could think of - the QR25DE power plant";

    public String  headingxtrail2="Cylinder";
    public String subheadingxtrail2= "The Nissan X-TRAIL powers with full performance from the 4-cylinder 2.5L engine developing 170 hp at 6,000 rpm and 23.8kg-m at 4,000 rpm, and featuring the CVTC variable valve system for high torque output at low revs and improved fuel economy.";


    public String  headingxtrail3="Transmissions";
    public String subheadingxtrail3="Superior to conventional automatic transmissions with seamless acceleration, low power loss and maintenance cost, along with ASC Logic that has 700 different gear change patterns for every driving situation in the Nissan X-TRAIL.";

    public String  headingxtrail4="All-Mode";
    public String subheadingxtrail4="With X-TRAIL’s All-Mode 4x4-i, you’ve got a system that’s capable on-road and off. You can choose full-time 2WD for maximum efficiency. Auto Mode constantly monitors conditions and adjusts the balance of power between front and rear wheels for best traction. Challenging conditions? 4WD Lock Mode is your choice.";


    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeaturePerformanceFragment newInstance(int  resId, String carName) {
        VehicleCarFeaturePerformanceFragment fragment = new VehicleCarFeaturePerformanceFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, resId);
        args.putString(ARG_PARAM2, carName);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_carfeatures_performance, container, false);

         carName = (TextView) view.findViewById(R.id.carName);
        carFeatureName = (TextView) view.findViewById(R.id.carFeatureName);
        recycler_view_performance = (RecyclerView) view.findViewById(R.id.recycler_view_performance);

        if(performanceCarArrayList.size()>0)
        {
            performanceCarArrayList.clear();;
        }

        carName.setText(mParam2);
        carFeatureName.setText("PERFORMANCE");

         if(mParam2.equalsIgnoreCase("JUKE")) {
             Performance performance1 = new Performance();
             performance1.setHeading(headingJuke1);
             performance1.setSubHeading(subheadingJuke1);
             performanceCarArrayList.add(performance1);

             Performance performance2 = new Performance();
             performance2.setHeading(headingJuke2);
             performance2.setSubHeading(subheadingJuke2);
             performanceCarArrayList.add(performance2);


             vehicleCarFeaturePerformanceAdapter = new VehicleCarFeaturePerformanceAdapter(performanceCarArrayList);
             RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
             recycler_view_performance.setLayoutManager(mLayoutManager);
             recycler_view_performance.setItemAnimator(new DefaultItemAnimator());
             recycler_view_performance.setAdapter(vehicleCarFeaturePerformanceAdapter);

         }
        else
         {
             Performance performance1 = new Performance();
             performance1.setHeading(headingxtrail1);
             performance1.setSubHeading(subheadingxtrail1);
             performanceCarArrayList.add(performance1);

             Performance performance2 = new Performance();
             performance2.setHeading(headingxtrail2);
             performance2.setSubHeading(subheadingxtrail2);
             performanceCarArrayList.add(performance2);

             Performance performance3 = new Performance();
             performance3.setHeading(headingxtrail3);
             performance3.setSubHeading(subheadingxtrail3);
             performanceCarArrayList.add(performance3);

             Performance performance4 = new Performance();
             performance4.setHeading(headingxtrail4);
             performance4.setSubHeading(subheadingxtrail4);
             performanceCarArrayList.add(performance4);


             vehicleCarFeaturePerformanceAdapter = new VehicleCarFeaturePerformanceAdapter(performanceCarArrayList);
             RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
             recycler_view_performance.setLayoutManager(mLayoutManager);
             recycler_view_performance.setItemAnimator(new DefaultItemAnimator());
             recycler_view_performance.setAdapter(vehicleCarFeaturePerformanceAdapter);
         }











        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
