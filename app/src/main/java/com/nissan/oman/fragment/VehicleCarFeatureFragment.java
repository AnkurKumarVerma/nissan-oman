package com.nissan.oman.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nissan.oman.R;

import com.nissan.oman.adapter.VehicleCarFeatureGridViewAdapter;
import com.nissan.oman.model.HomeItem;
import com.nissan.oman.model.Result;
import com.nissan.oman.model.ResultVehicle;
import com.nissan.oman.touchEvent.ClickListenerInterface;
import com.nissan.oman.touchEvent.RecycleviewTouch;
import com.nissan.oman.utils.SharedPrefKeys;
import com.nissan.oman.utils.SharedPreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleCarFeatureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeatureFragment extends Fragment {



    private VehicleCarFeatureGridViewAdapter vehicleCarFeatureGridViewAdapter;

    private RecyclerView recyclerView;

   // private  int iconArray[]={R.drawable.vehicles,R.drawable.vehicles,R.drawable.vehicles,R.drawable.vehicles,R.drawable.vehicles,R.drawable.vehicles,R.drawable.vehicles,R.drawable.vehicles};
    private String headingArray[]={"OVERVIEW","EXTERIOR","INTERIOR","PERFORMANCE","SAFETY","COLOR AND TRIM","VERSATILITY","SPECIFICATION","GALLERY","TECHNOLOGY","REQUEST A BROCHURE"};
    private  ArrayList<HomeItem> carFeatureArrayList=new ArrayList<>();




    private GridLayoutManager gridLayoutManagerVertical;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private int mParam1;
    private String mParam2;
    private String mParam4;
    private int mParam3;
    private ImageView iconCar;
    private SharedPreferenceUtil sharedPreferenceUtil;


    public VehicleCarFeatureFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeatureFragment newInstance(int resId, String carName, int carId, String mParam2) {
        VehicleCarFeatureFragment fragment = new VehicleCarFeatureFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1,resId);
        args.putInt(ARG_PARAM3,carId);
        args.putString(ARG_PARAM2, carName);
        args.putString(ARG_PARAM4,mParam2);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getInt(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_carfeatures, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_carfeature);
        iconCar = (ImageView) view.findViewById(R.id.iconCar);
        iconCar.setImageResource(mParam1);

        TextView carName = (TextView)view.findViewById(R.id.carName);
        carName.setText(mParam2);

        TextView carFeatureName = (TextView)view.findViewById(R.id.carFeatureName);
        carFeatureName.setText(mParam4);


//        sharedPreferenceUtil = SharedPreferenceUtil.getInstance(getActivity());
//        String vehicleData= sharedPreferenceUtil.getData(SharedPrefKeys.VEHICLE_DATA,"");
//        Gson gson = new Gson();
//        JSONObject vehicleobj;
//        try {
//            vehicleobj = new JSONObject(vehicleData);
//            ResultVehicle result = gson.fromJson(vehicleobj.toString(), ResultVehicle.class);
//            System.out.println("result" +result);
//        } catch (JSONException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }



            if(carFeatureArrayList.size()>0)
        {
            carFeatureArrayList.clear();;
        }
        for(int i=0;i<headingArray.length;i++)
        {
            HomeItem passengerCarItem=new HomeItem();

            passengerCarItem.setHeading(headingArray[i]);
            carFeatureArrayList.add(passengerCarItem);

        }



        vehicleCarFeatureGridViewAdapter = new VehicleCarFeatureGridViewAdapter(carFeatureArrayList,getActivity());
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        gridLayoutManagerVertical =
                new GridLayoutManager(getActivity(),
                        2, //The number of Columns in the grid
                        LinearLayoutManager.VERTICAL,
                        false);

        // int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.grid_margin);
        //recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));

        gridLayoutManagerVertical.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if(position==(carFeatureArrayList.size()-1)){
                    return 2;

                }
                else
                {
                    return 1;
                }
            }
        });

        recyclerView.setLayoutManager(gridLayoutManagerVertical);

        recyclerView.setAdapter(vehicleCarFeatureGridViewAdapter);

        recyclerView.addOnItemTouchListener(new RecycleviewTouch(getActivity(), recyclerView, new ClickListenerInterface() {
            @Override
            public void onClick(View view, int position) {
                //view.setBackgroundColor(getResources().getColor(R.color.btn_red));
                if (position == 0) {

                    VehicleCarFeatureOverviewFragment vehicleCarFeatureOverviewFragment =VehicleCarFeatureOverviewFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureOverviewFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }
                 else if (position == 1) {

                    VehicleCarFeatureExteriorFragment vehicleCarFeatureExteriorFragment =VehicleCarFeatureExteriorFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureExteriorFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }


                else if (position == 2) {

                    VehicleCarFeatureInteriorFragment vehicleCarFeatureInteriorFragment =VehicleCarFeatureInteriorFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureInteriorFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }

                else if (position == 3) {

                    VehicleCarFeaturePerformanceFragment vehicleCarFeaturePerformanceFragment =VehicleCarFeaturePerformanceFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeaturePerformanceFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }

                else if (position == 4) {

                    VehicleCarFeatureSafetyFragment vehicleCarFeatureSafetyFragment =VehicleCarFeatureSafetyFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureSafetyFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }

                else if (position == 5) {

                    VehicleCarFeatureBodyColorFragment vehicleCarFeatureBodyColorFragment =VehicleCarFeatureBodyColorFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureBodyColorFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }


                else if (position == 7) {

                    VehicleCarFeatureSpecificationFragment vehicleCarFeatureSpecificationFragment =VehicleCarFeatureSpecificationFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureSpecificationFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }

                else if (position == 6) {

                    VehicleCarFeatureVersatilityFragment vehicleCarFeatureVersatilityFragment =VehicleCarFeatureVersatilityFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureVersatilityFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }


                else if (position == 8) {

                    VehicleCarFeatureGalleryFragment vehicleCarFeatureGalleryFragment =VehicleCarFeatureGalleryFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureGalleryFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }

                else if (position == 9) {

                    VehicleCarFeatureTechnologyFragment vehicleCarFeatureTechnologyFragment =VehicleCarFeatureTechnologyFragment.newInstance(mParam1,mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureTechnologyFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }


                else if (position == 10) {

                    VehicleCarFeatureBrochureFragment vehicleCarFeatureBrochureFragment =VehicleCarFeatureBrochureFragment.newInstance(mParam1+"",mParam2);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureBrochureFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }
                // Toast.makeText(getActivity(), position + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
       // mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
