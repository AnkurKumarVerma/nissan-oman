package com.nissan.oman.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nissan.oman.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehicleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehicleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehicleCarFeatureInteriorFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;




    public VehicleCarFeatureInteriorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment VehicleFragment.
     *
     *
     */

    private TextView carName,carFeatureName,carDescription;
    private ImageView carImage;
    private TextView btnExterior;
    private TextView btnInterior;


    // TODO: Rename and change types and number of parameters
    public static VehicleCarFeatureInteriorFragment newInstance(int  resId, String carName) {
        VehicleCarFeatureInteriorFragment fragment = new VehicleCarFeatureInteriorFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, resId);
        args.putString(ARG_PARAM2, carName);
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_carfeatures_exterior, container, false);
         carImage = (ImageView) view.findViewById(R.id.carImage);
         carName = (TextView) view.findViewById(R.id.carName);
        carFeatureName = (TextView) view.findViewById(R.id.carFeatureName);
        carDescription = (TextView) view.findViewById(R.id.carDescription);


        carImage.setImageResource(mParam1);
        carName.setText(mParam2);
        carFeatureName.setText("CROSS OVER");
        carDescription.setText("HOVER OVER TO VIEW DETAIL");


        btnExterior = (TextView) view.findViewById(R.id.btnExterior);
        btnInterior = (TextView) view.findViewById(R.id.btnInterior);


        if(mParam2.equalsIgnoreCase("JUKE"))
        {
            carImage.setImageResource(R.drawable.juke2);
        }
        else
        {
            carImage.setImageResource(R.drawable.xtrail2);
        }
        carName.setText(mParam2);
        carFeatureName.setText("CROSS OVER");
        carDescription.setText("HOVER OVER TO VIEW DETAIL");
        btnInterior.setSelected(true);
        btnInterior.setFocusable(true);

        btnExterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int interior;
                if(mParam2.equalsIgnoreCase("JUKE"))
                {
                    interior =R.drawable.juke3;
                }
                else
                {
                    interior=R.drawable.xtrail3;
                }

                carImage.setImageResource(interior);

//                VehicleCarFeatureExteriorFragment vehicleCarFeatureExteriorFragment =VehicleCarFeatureExteriorFragment.newInstance(interior,mParam2);
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.realtabcontent, vehicleCarFeatureExteriorFragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();


            }
        });


        btnInterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mParam2.equalsIgnoreCase("JUKE"))
                {
                    carImage.setImageResource(R.drawable.juke2);
                }
                else
                {
                    carImage.setImageResource(R.drawable.xtrail2);
                }


            }
        });






        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
